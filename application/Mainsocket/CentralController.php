<?php
/**
 * Main WebSocket Controller
 */
namespace Mf\Mainsocket;
use Mf\WebSocket\Event\Event;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\User;
use Mf\Mainsocket\LoveInteractionController;
use Mf\Mainsocket\library\Mainsocket\YanPalUser;
use Mf\Mainsocket\library\Mainsocket\UserHelper;

class CentralController extends \Mf\WebSocket\Event\Observer
{
	// Main database object
	protected $_dbMain;
	protected $_UserDb;



	// Invidual users
	protected $_users = array();


	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->_dbMain = Registry::getInstance()->get('MainDb');
		$this->_UserDb = Registry::getInstance()->get('UserDb');


	}


	public function onConnect(Event $event)
	{
		$server = $event->getTarget();
		$client = $event->getParam('client');
		$ip = $client->ip;

		$msg = "Client $ip has connected";
		$server->log($msg);
	}

	/**
	 * - Remove user from chat
	 * - Log disconnected user
	 */
	public function onDisconnect(Event $event)
	{
		$server = $event->getTarget();
		$client = $event->getParam('client');
		$socket = $client->socket;
		$ip = $client->ip;

		$msg = "Client $ip has disconnected";
		$server->log($msg);

		// Remove disconnected user by socket
		foreach($this->_users as $user)
		{
			if($socket === $user->socket)
			{
				$this->onUserLeave($user, $event);
				break;
			}
		}
	}

	/**
	 * - Check that it's supported message
	 * - Notify connected users
	 * - Save message in database
	 */
	public function onMessage(Event $event)
	{
		$server = $event->getTarget();
		$client = $event->getParam('client');
		$message = json_decode($event->getParam('message'));


		if(!$message) {
			var_dump('Message Error');
			return false;
		}



		if($message->action === 'user_join'){
			$this->onUserJoin($message, $event);
		}






		if($message->action === 'loveInteraction'){
				$love = new LoveInteractionController($message, $event);
				$love-> execute($message, $event);
				$returnedData = $love->build();
				$this->GeneralResponse($returnedData, $event);
		}


	}//ends on message

	public function onUserJoin($message, $event)
	{
		$userId = (isset($message->userId))?$message->userId: null;
		$server = $event->getTarget();
		$client = $event->getParam('client');
		$server->log(json_encode($message));

		if($userId == null){
				$server->log(json_encode(array("status"=>"userId undefined")));
		}

		// fetch user details
		$properties = $this->_UserDb->fetchUser($userId);
		if((!$properties)) {
			//@Error:
			$msg = json_encode(array(

				'message' => 'User does not exist',
				"controler" => "interaction",
				"action" => "user_join"
			));
			$server->send($client->socket, $msg);
			//$server->log($msg);
			return false;
		}
		$newUser = new YanPalUser(get_object_vars($properties));
		$userObject = new YanPalUser(get_object_vars($properties));
		$newUser->socket = $client->socket;
		$newUser->ip = $client->ip;
		$this->addUser($newUser);
		$msg2 = json_encode(array(
			"controler" => "interaction",
			"action" => "user_join",
			"user" => $userObject
		));
		//$server->send($client->socket, $msg2);

		foreach($this->_users as $use){
			if($use->userId == $userId){
				$server->send($use->socket, $msg2);
			}
		}

		//$server->log($msg2);

	}




	/**
	 * - Broadcast user that has left chat
	 * - Remove use from users
	 */
	public function onUserLeave($user, $event)
	{
		$userIndex = -1;
		foreach($this->_users as $index => $tmpUser)
		{
			if($user == $tmpUser) $userIndex = $index;
		}

		if($user == false || $userIndex == -1) return false;

		$server = $event->getTarget();
		$client = $event->getParam('client');

		// Unset socket, ip
		unset($user->ip, $user->socket);

		// Prepare message
		$msg = json_encode(array(
			'controller' => 'interaction',
			'action' => 'user_leave',
			'user' => $user
		));

		// Remove user
		if($this->removeUser($userIndex))
		{
			// Broadcast disconnected user to all other connected users
			foreach($this->_users as $recvr)
			{
				if($user !== $recvr || !!$recvr->socket)
					$server->send($recvr->socket, $msg);
			}
		} else {
			return false;
		}

		return true;
	}





	/**
	 * Adds user to chat
	 */
	public function addUser($user)
	{
		array_push($this->_users, $user);
	}

	/**
	 * Removes a user from chat
	 */
	public function removeUser($index)
	{
		if($index > -1 && $index < count($this->_users))
		{
			array_splice($this->_users, $index, 1);
			return true;
		}

		return false;
	}

	/**
	 *
	 */
	public function getUserById($id)
	{
		foreach($this->_users as $user)
		{
			if($user->userId === $id)
			{
				return $user;
			}
		}

		return null;
	}



	public function GeneralResponse($returnedData, $event)//sends the data to those who are concerned and online ;
  {
  $server = $event->getTarget();
 	$client = $event->getParam('client');

 	$UserPalsId = $returnedData["users"];
 	$msg = json_encode($returnedData["msg"]);


 		foreach($UserPalsId as $one){

 			foreach($this->_users as $use){
 				if($use->userId == $one){
 					$server->send($use->socket, $msg);
 				}
 			}//ends second foreach
 	}//ends first foreach
  }//end of function







}//end of class
