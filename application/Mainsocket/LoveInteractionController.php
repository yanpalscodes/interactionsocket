<?php
namespace Mf\Mainsocket;
use \GearmanClient;
use \GearmanTask;
use Mf\WebSocket\Event\Event;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\library\Mainsocket\GeneralFunctions;
use Mf\Mainsocket\library\Mainsocket\Moment;
use Mf\Mainsocket\library\Mainsocket\Comment;
use Mf\Mainsocket\library\Mainsocket\YanPalUser;


Class LoveInteractionController {

	protected $event;
	protected $message;
	protected $_UserDb;
	protected $_MomentDb;
	protected $_NotificationDb;
	public $returnedData = array();
	protected $_listerners;



	public function __construct()
	{
		$this->_UserDb = Registry::getInstance()->get('UserDb');
		$this->_MomentDb = Registry::getInstance()->get('MomentDb');
		$this->_CommentDb = Registry::getInstance()->get('CommentDb');
		$this->_NotificationDb = Registry::getInstance()->get('NotificationDb');
	}



	public function execute($message, $event)
	{
				$startTime = microtime(true);
				$this->server = $server = $event->getTarget();
				$client = $event->getParam('client');
				$this->generalFunctions = new GeneralFunctions();
				$momentId = $message->momentId;
				$userId = $message->userId;
				$type = $message->type;

				//fetch the moment
				$momentProperties = $this->_MomentDb->getMomentByMomentId($momentId);


					if($type == 'love'){// if it is love

									if($this->_MomentDb->checkIfLoved($momentId,$userId) == false){//if the person has not love before

												$lastLoveId = $this->_MomentDb->storeInteraction($userId, $momentId, 1);// note that 1 means love //store in interaction table and get the last inserid
												if($userId != $momentProperties["UserId"]){// if it is not the owner of the moment that loved the moment

														$lastNotId = $this->_MomentDb->storeInteractionNotification($momentProperties["UserId"], $userId, $momentProperties["UserId"], $lastLoveId);

															if($lastNotId != 'error'){// if there was no error in the insert
																	$notificationType = $this->generalFunctions->NotificationType('moment');
																	$insertResult = $this->_NotificationDb->insertIntoNotification($userId, $notificationType, 	$lastNotId, time(), 0);
													    }//if there is no error
											 }// end second if

										}
										else{
											$this->_MomentDb->trashLoveInteraction($momentId,$userId, 1);// delete it
										}

				 	}//end first if

						$this->momentOwnerUserId  = $momentProperties["UserId"];

						$theMoment  = new Moment($momentProperties, $userId, false);//false means it will not fetch comments

						//now fetch the followers of the moment owner so as to send the comment to them
						$userFollowersIds  = array();
						$usersFollowersFromDd = $this->_UserDb->fetchUserFollowers($this->momentOwnerUserId);

						foreach ($usersFollowersFromDd as $key) {
							$userFollowersIds[] = $key["FollowerId"];
						}

						/**
						* if the owner of the moment  is not the person making the action  add him/her to the list of people
						* that will recieve the update
						*/
							if($this->momentOwnerUserId != $userId){
								$userFollowersIds[] = $this->momentOwnerUserId;
							}

							$userFollowersIds[] = $userId;
							$userFollowersIds = array_unique($userFollowersIds);//remove repeated userids

							$loveCount = $this->_MomentDb->loveCounts($momentId);

							//data sent back to central controller to be distributed
							$this->returnedData  = array(
							"msg" => array("action"=>"loveInteraction", "controller" => "interaction", "status" => "ok", "loveCount"=>$loveCount,
							"momentId"=>$momentId, "userId"=>$userId, "tag"=>$message->tag,
							"moment" => $theMoment),
							"users" => $userFollowersIds,
						);



	}//ends function




	public function build()
	{
		return $this->returnedData;
	}


}//ends class



?>
