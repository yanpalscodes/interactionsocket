<?php

use Mf_Core\Database\Database;
use Mf_Core\Database\Driver\Pdo;
use Mf_Core\Config\Config;




Class DatabaseComments extends Database
{
	public function __construct()
	{
		$config = Config::getInstance();
		$dbConfig = $config->get('maindatabase');

		try {
			$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
			parent::__construct('wokondb', $driver);
		} catch (Exception $ex) {
			exit('Database connection error');
		}
	}






	public function getCommentById($commentId){
			$query = $this->_driver->prepare("select * from Comments where CommentId = :commentId");
			$query->bindValue(':commentId', $commentId, PDO::PARAM_STR);
			$query->execute();
			$rows = $query->fetch(PDO::FETCH_ASSOC);
			$query = NULL;
			return $rows;
	}

public function countMomentComments($momentId)
{

		$que = $this->_driver->prepare('SELECT count(*) as count from Comments where MomentId =:momentId and Type = 1');
		$que->bindValue(':momentId', $momentId, PDO::PARAM_STR);
		$que->execute();
		$que->bindColumn('count', $count);
		$que->fetch(PDO::FETCH_ASSOC);
		$que = NULL;
		return $count;

}

	public function insertComment($userId, $momentId, $content, $time, $type, $originalCommentId, $mediaId, $commentId)
	{
		$st = $this->_driver->prepare("INSERT INTO Comments
			 (UserId, MomentId, Content, Time, Type, OriginalCommentId, MediaId, CommentId)
		 values (:userId, :momentId, :content, :time, :type, :originalCommentId, :mediaId, :commentId) ");
		$st->bindValue(':commentId', $commentId, PDO::PARAM_STR);
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':content', $content, PDO::PARAM_STR);
		$st->bindValue(':time', $time, PDO::PARAM_STR);
		$st->bindValue(':originalCommentId', $originalCommentId, PDO::PARAM_STR);
		$st->bindValue(':type', $type, PDO::PARAM_INT);
		$st->bindValue(':mediaId', $mediaId, PDO::PARAM_STR);
		$st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
		return ($st->execute()) ? true : false;
	}

	public function getEligibleNotifiersForComment($momentId)
	{
		$st = $this->_driver->prepare("select SubscriberId from MomentSubscribers where MomentId =:momentId and Status = 1");
		$st->bindValue(':momentId',$momentId, PDO::PARAM_STR);
		$st->execute();
		return $st->fetchAll(PDO::FETCH_ASSOC);
	}


public function getMediaDetailsByCommentId($commentId)
{
	$st = $this->_driver->prepare("SELECT * FROM Media where SourceId =:commentId");
	$st->bindValue(':commentId', $commentId, PDO::PARAM_STR);
	$st->execute();
	return $st->fetch(PDO::FETCH_ASSOC);
}

public function getMediaDetailsByMediaId($mediaId)
{
	$st = $this->_driver->prepare("SELECT * FROM Media where MediaId =:mediaId");
	$st->bindValue(':mediaId', $mediaId, PDO::PARAM_STR);
	$st->execute();
	return $st->fetch(PDO::FETCH_ASSOC);
}

public function countCommentsReply($commentId)
{
	$que = $this->_driver->prepare('SELECT count(*) as count from Comments where TypeId =:commentId and Type = 2');
	$que->bindValue(':commentId', $commentId, PDO::PARAM_STR);
	$que->execute();
	$que->bindColumn('count', $count);
	$que->fetch(PDO::FETCH_ASSOC);
	$que = NULL;
	return $count;
}

	public function getEligibleNotifiersForCommentReplys($commentId)
	{
		$que = $this->_driver->prepare('SELECT distinct UserId from UserComment where TypeId =:commentId and Type = 2');
		$que->bindValue(':commentId', $commentId, PDO::PARAM_STR);
		$que->execute();
		$row = $que->fetchAll(PDO::FETCH_ASSOC);
		return $row;
	}


/**
* This method will fake as if it is deleting a comment by changing the trashed value to 1, by so doing,
* the comment will not be fetched while fetching comments
*/
		public function deleteComment($commentId)
		{
			$que = $this->_driver->prepare('UPDATE  UserComment set Trashed = 1 where CommentId =:commentId');
			$que->bindValue(':commentId', $commentId, PDO::PARAM_STR);
			if($que->execute()){ return true;}else{ return false;}
		}



}//ends class
