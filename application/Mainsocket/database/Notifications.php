<?php
	use \Mf_Core\Registry;
	use Mf_Core\Database\Database;
	use Mf_Core\Database\Driver\Pdo;
	use Mf_Core\Config\Config;


Class DatabaseNotifications extends Database{



		public function __construct()
		{
			$config = Config::getInstance();
			$dbConfig = $config->get('maindatabase');

			try {
				$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
				parent::__construct('wokondb', $driver);
			} catch (Exception $ex) {
				exit('Database connection error');
			}


	  }



		public function insertIntoCommentNotification($notificationOwnerId, $notificationInitiatorId, $contentCreatorId, $commentId)
		{
			$st = $this->_driver->prepare("INSERT INTO CommentNotification (NotificationOwnerId, NotificationInitiatorId, ContentCreatorId, CommentId)
			VALUES (:notificationOwnerId, :notificationInitiatorId, :contentCreatorId,  :commentId)");
			$st->bindValue(':notificationOwnerId', $notificationOwnerId, PDO::PARAM_STR);
			$st->bindValue(':notificationInitiatorId', $notificationInitiatorId, PDO::PARAM_STR);
			$st->bindValue(':contentCreatorId',$contentCreatorId, PDO::PARAM_STR);
			$st->bindValue(':commentId', $commentId, PDO::PARAM_STR);
			$st->execute();
			return $this->_driver->lastInsertId();
		}


		public function insertIntoNotification($userId, $type, $typeId, $time, $status)
		{
			$st = $this->_driver->prepare("INSERT INTO Notification (UserId, Type, TypeId, Time, Seen)
			VALUES (:userId, :type, :typeId, :time, :seen)");
			$st->bindValue(':userId', $userId, PDO::PARAM_STR);
			$st->bindValue(':type', $type, PDO::PARAM_INT);
			$st->bindValue(':typeId',$typeId, PDO::PARAM_STR);
			$st->bindValue(':time', $time, PDO::PARAM_STR);
			$st->bindValue(':seen', $status, PDO::PARAM_INT);
			return ($st->execute()) ? true : false;
		}




    public function getUserNotifications($userId, $limit, $offset){
		$st = $this->_driver->prepare("select * from `Notification` where `UserId` = :userId   order by DateTimeLogged desc LIMIT :offset, :limits ");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':limits', $limit, PDO::PARAM_INT);
		$st->bindValue(':offset', $offset, PDO::PARAM_INT);

		$st->execute();
		$result = $st->fetchAll(PDO::FETCH_ASSOC);
		$st = NULL;
		return $result;
	}






	public function loadMoreNotifications($userId, $limit, $boffset)
	{
		$st = $this->_driver->prepare("select * from `Notification` where `UserId` = :userId  and DateTimeLogged < :Boffset
		order by DateTimeLogged desc LIMIT  :limits ");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':limits', $limit, PDO::PARAM_INT);
		$st->bindValue(':Boffset', $boffset, PDO::PARAM_INT);

		$st->execute();
		$result = $st->fetchAll(PDO::FETCH_ASSOC);
		$st = NULL;
		return $result;
	}


	public function loadRecentNotifications($userId, $limit, $toffset)
	{
		$st = $this->_driver->prepare("select * from `Notification` where `UserId` = :userId  and DateTimeLogged > :Toffset
		order by DateTimeLogged desc LIMIT :limits ");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':limits', $limit, PDO::PARAM_INT);
		$st->bindValue(':Toffset', $toffset, PDO::PARAM_INT);

		$st->execute();
		$result = $st->fetchAll(PDO::FETCH_ASSOC);
		$st = NULL;
		return $result;
	}




	public function countUserNotifications($userId){//counts all the notifications of a user
		$st = $this->_driver->prepare("CALL sp_CountUserNotification(:userId)");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn('count', $count);
		$result = $st->fetch(PDO::FETCH_ASSOC);
		$st = NULL;
		return $count;
	}


	public function countUserUnseenNotifications($userId){//counts all the notifications which a user has not seen
		$st = $this->_driver->prepare("CALL sp_CountUserUnreadNotification(:userId)");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->execute();
		$st->bindColumn('count', $count);
		$result = $st->fetch(PDO::FETCH_ASSOC);
		$st = NULL;
		return $count;
	}





}
