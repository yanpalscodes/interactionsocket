<?php

defined('_PUBLIC') || exit;

use Mf_Core\Database\Database;
use Mf_Core\Database\Driver\Pdo;
use Mf_Core\Config\Config;



Class DatabaseUser extends Database
{
	public function __construct()
	{
		$config = Config::getInstance();
		$dbConfig = $config->get('maindatabase');

		try {
			$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
			parent::__construct('wokondb', $driver);
		} catch (Exception $ex) {
			exit('Database connection error');
		}
	}

	public function fetchUser($userId)
	{
		return $this->query("select u.*, b.* from UserCredentials
		 as u inner join UserBioData as b on u.UserId = b.UserId where u.UserId = :userId", array("userId" => $userId));
	}

	public function countUserFollowers($userId)
	{
		$count = $this->bindFetch("select count(*) as count  from Follows where FollowingId =:userId and Status = 1 ", array("userId" => $userId), array('count'))['count'];
		return $count;
	}

	public function updateUserInfo($fieldName, $fieldValue, $userId)
	{
		# code...
		$this->update('UserBioData', array($fieldName => $fieldValue), array('UserId' => $userId));
	}


	public function fetchUserInterests($userId)
	{
		$sql = $this->_driver->prepare("SELECT InterestId FROM UserInterest where UserId =:userId ");
		$sql->bindValue(":userId", $userId, PDO::PARAM_STR);
		$sql->execute();
		return $sql->fetchAll(PDO::FETCH_ASSOC);
	}


	public function fetchUserFollowers($userId)
	{
		$sql = $this->_driver->prepare("SELECT FollowerId FROM Follows where FollowingId =:userId ");
		$sql->bindValue(":userId", $userId, PDO::PARAM_STR);
		$sql->execute();
		return $sql->fetchAll(PDO::FETCH_ASSOC);
	}






	/**
	 * @author Imoh
	 * Execute a query string
	 * @param String $queryStr : The query String
	 * @param Array $paramBinding : An Associative array of the parameters with thier corresponding values
	 * @param String $fetchType : flag to dertermine weather to return a single or multiple fetch from the DB
	 * @return PDOObject
	 */
	public function query( $queryStr, Array $parambinding = array(), $fetchMultipleRows = false ){

		$st = $this->_driver->prepare($queryStr);
		if(!empty($parambinding)){
			foreach($parambinding as $key => $value){
				if(ctype_digit($value)){
					$st->bindValue(':' . $key, $value, PDO::PARAM_INT);
				}else {
					$st->bindValue(':' . $key, $value, PDO::PARAM_STR);
				}
			}
		}
		if($fetchMultipleRows){
			return $st->execute() ? $st->fetchAll(PDO::FETCH_OBJ) : null;
		}
		return $st->execute() ? $st->fetch(PDO::FETCH_OBJ) : null;

	}

	/**
	 * @author Imoh
	 * Insert records into the database
	 * @params tbl : table name to insert into
	 * @param Array params : a key-value pair array where the keys are database fields to be inserted
	 * @return bool
	 */

	public function insert($tbl, Array $params){

		# check if dbName exist
		$this->_checkTableExist($tbl);

		// setup some variables for Query, fields and values
		$fields  = "";
		$values = "";

		// populate them
		foreach ($params as $f => $v){
			$fields  .= " `$f`,";
			$values .= " :$f ,";
		}
		// remove our trailing ,
		$fields = substr($fields, 0, -1);
		// remove our trailing ,
		$values = substr($values, 0, -1);

		$queryStr = "INSERT INTO `$tbl` ({$fields}) VALUES({$values})";

		$st = $this->_driver->prepare($queryStr);
		//bind Parameters
		foreach($params as $field => $value){
			if(ctype_digit($value)){
				$st->bindValue(':' . $field, $value, PDO::PARAM_INT);
			}else {
				$st->bindValue(':' . $field, $value, PDO::PARAM_STR);
			}
		}
		try {
			$st->execute();
			$st = NULL;

			return true;
		}catch (PDOException $e){
			var_dump($e->errorInfo);
		}
	}


	/**
	 * @param $sp
	 * @param array $params
	 * @param array $bindings
	 * @return array
	 * @throws \Mf_Core\Upload\Exception
	 */
	public function bindFetch($queryStr, Array $params, Array $bindings){

		$st = $this->_driver->prepare($queryStr);

		# bind parameters
		if(!empty($params)){
			foreach($params as $key => $value){
				if(ctype_digit($value)){
					$st->bindValue(':' . $key, $value, PDO::PARAM_INT);
				}else {
					$st->bindValue(':' . $key, $value, PDO::PARAM_STR);
				}
			}
		}

		$st->execute();


		# for each column binding, bind column
		foreach($bindings as $binding){
			$st->bindColumn($binding, $$binding);
		}

		$st->fetch(PDO::FETCH_ASSOC);

		# build response
		$response = array();

		foreach($bindings as $binding){
			$response[$binding] = $$binding;
		}

		return $response;

	}




}
