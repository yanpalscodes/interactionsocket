<?php


use \Mf_Core\Registry;
use Mf_Core\Database\Database;
use Mf_Core\Database\Driver\Pdo;
use Mf_Core\Config\Config;



class DatabaseMoments extends Database{



	public function __construct()
	{
		$config = Config::getInstance();
		$dbConfig = $config->get('maindatabase');

		try {
			$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
			parent::__construct('wokondb', $driver);
		} catch (Exception $ex) {
			exit('Database connection error');
		}


  }



	public function storeMoment(array $data)
	{
		return $this->insert('Moments', $data);
	}

	public function storeMedia($data)
	{
	 return $this->insert('Media', $data);
	}

	public function matchInterests($momentId, array $foundInterests) {
			$sql = "SELECT * FROM wokondb.Interest JOIN wokondb.InterestCategory ON wokondb.Interest.Id = wokondb.InterestCategory.Id WHERE ";
			 $i = count($foundInterests); $max = $i - 1;
			//foreach($foundInterests as $foundInterest) {
			for($x = 0; $x < $i; $x++){

					$oneInterst = $foundInterests[$x];
					$sql .= " InterestCategory.Category LIKE '%$oneInterst%' OR Interest.Name LIKE '%$oneInterst%'";
					if($x < $max) {
							$sql .= " OR";
					}

			}


			$response = $this->query($sql,  array(), true);
			$interestCats = [];

			if(count($response) > 0) {

					foreach ($response as $res) {
							$interestCats[] = $res->InterestCategoryId;
					}

					// make unique
					$unique_array = array_unique($interestCats);

					foreach($unique_array as $unique) {
							$this->query("INSERT INTO wokondb.MomentInterest SET MomentId = :momentId, InterestId = :interestId", array('momentId' => $momentId, 'interestId' => $unique));
					}
					return true;
			} else {
					$this->query("INSERT INTO wokondb.MomentInterest SET MomentId = :momentId, InterestId = :interestId", array('momentId' => $momentId, 'interestId' => 0));
					return false;
			}
	}



public function fetchUserMomentsOnFirstCall($userId, $limit)
{
	$sql = $this->_driver->prepare("SELECT * FROM Moments where (UserId =:userId OR UserId IN (SELECT FollowingId FROM Follows WHERE FollowerId =:userId AND Status = 1))  AND (Trashed = 0 ) ORDER BY TIME DESC LIMIT  :limit");
	$sql->bindValue(":userId", $userId, PDO::PARAM_STR);
	$sql->bindValue(":limit", $limit, PDO::PARAM_INT);
	$sql->execute();
	return $sql->fetchAll(PDO::FETCH_ASSOC);
}

public function fetchUserMomentsOnSubsequentCalls($userId, $limit, $lastMomentId)
{
	$sql = $this->_driver->prepare("SELECT * FROM Moments where (UserId =:userId OR UserId IN (SELECT FollowingId FROM Follows WHERE FollowerId =:userId AND Status = 1))  AND (Trashed = 0 )
	AND (ID < :id )  ORDER BY TIME DESC LIMIT  :limit");
	$sql->bindValue(":userId", $userId, PDO::PARAM_STR);
	$sql->bindValue(":limit", $limit, PDO::PARAM_INT);
	$sql->bindValue(":id", $lastMomentId, PDO::PARAM_INT);
	$sql->execute();
	return $sql->fetchAll(PDO::FETCH_ASSOC);
}

public function fetchMomentIntersts($momentId)
{
	$sql = $this->_driver->prepare("SELECT InterestId from MomentInterest WHERE MomentId =:momentId ");
	$sql->bindValue(":momentId", $momentId, PDO::PARAM_STR);
	$sql->execute();
	return $sql->fetchAll(PDO::FETCH_ASSOC);

}


public function checkStoppedSubscription($momentId, $userId)
{
	$st = $this->_driver->prepare("select count(*) as count from MomentSubscribers where SubscriberId =:userId and
  Status = 0  and MomentId =:momentId");
  $st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
  $st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->execute();
  $st->bindColumn('count', $count);
  $result = $st->fetch(PDO::FETCH_ASSOC);
  return ($count > 0)? true : false;
}



public function loveCounts($momentId)
{
	$st = $this->_driver->prepare("select count(*) as count from Interaction where MomentId =:momentId");
  $st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
  $st->execute();
  $st->bindColumn('count', $count);
	$st->fetch(PDO::FETCH_ASSOC);
  return $count;
}


public function checkIfRebroadcasted($momentId, $userId)
{
	$st = $this->_driver->prepare("select count(*) as count from Moments where OriginalMomentId =:momentId AND
	UserId =:userId");
  $st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
	$st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->execute();
  $st->bindColumn('count', $count);
	$st->fetch(PDO::FETCH_ASSOC);
	return ($count > 0)? true : false;
}

public function checkIfLoved($momentId,$userId)
{
	$st = $this->_driver->prepare("select count(*) as count from Interaction where MomentId =:momentId AND
	UserId =:userId AND InteractionType = 1 AND Trashed = 0");
  $st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
	$st->bindValue(':userId', $userId, PDO::PARAM_STR);
  $st->execute();
  $st->bindColumn('count', $count);
	$st->fetch(PDO::FETCH_ASSOC);
	return ($count > 0)? true : false;
}


public function trashLoveInteraction($momentId,$userId, $type)
{
		$st = $this->_driver->prepare("Delete from Interaction where MomentId =:momentId AND UserId =:userId AND InteractionType =:type");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
		$st->bindValue(':type', $type, PDO::PARAM_INT);
		return $st->execute() ? $st->fetch(PDO::FETCH_ASSOC) : null;
}

public function checkIfSubscriptionEnabled($userId, $momentId)
{
  $count = $this->bindFetch("select count(*) as count  from MomentSubscribers  where MomentId =:momentId AND SubscriberId =:userId AND Status =:status",
   array("momentId" => $momentId, "userId"=> $userId, "status"=>1), array('count'))['count'];
  if($count == 1){ return true;}else{ return false; }
}





public function countNumberOfRebroadcastForAMoment($momentId){

	$count = $this->bindFetch("select count(*) as count  from Moments where OriginalMomentId =:OriginalMomentId ", array("OriginalMomentId" => $momentId), array('count'))['count'];
	return $count;
}


public function getMediaByMomentId($momentId)
{
	$st = $this->_driver->prepare("SELECT * FROM Media where SourceId = :sourceId");
	$st->bindValue(':sourceId', $momentId, PDO::PARAM_STR);
	$st->execute();
	return $st->fetchAll(PDO::FETCH_ASSOC);
}

public function getMomentByMomentId($momentId)
{
	$st = $this->_driver->prepare("select * from Moments where MomentId = :momentId");
	$st->bindParam(':momentId', $momentId, PDO::PARAM_STR);
	return $st->execute() ? $st->fetch(PDO::FETCH_ASSOC) : null;
}

public function countMomentComments($momentId)
{
  $count = $this->bindFetch("select count(*) as count  from Comments  where MomentId =:momentId ", array("momentId" => $momentId), array('count'))['count'];
  return $count;
}



public function query( $queryStr, Array $parambinding = array(), $fetchMultipleRows = false ){

	$st = $this->_driver->prepare($queryStr);
	if(!empty($parambinding)){
		foreach($parambinding as $key => $value){
			if(ctype_digit($value)){
				$st->bindValue(':' . $key, $value, PDO::PARAM_INT);
			}else {
				$st->bindValue(':' . $key, $value, PDO::PARAM_STR);
			}
		}
	}
	if($fetchMultipleRows){
		return $st->execute() ? $st->fetchAll(PDO::FETCH_OBJ) : null;
	}
	return $st->execute() ? $st->fetch(PDO::FETCH_OBJ) : null;

}



public function insert($tbl, Array $params){



	// setup some variables for Query, fields and values
	$fields  = "";
	$values = "";

	// populate them
	foreach ($params as $f => $v){
		$fields  .= " `$f`,";
		$values .= " :$f ,";
	}
	// remove our trailing ,
	$fields = substr($fields, 0, -1);
	// remove our trailing ,
	$values = substr($values, 0, -1);

	$queryStr = "INSERT INTO `$tbl` ({$fields}) VALUES({$values})";

	$st = $this->_driver->prepare($queryStr);
	//bind Parameters
	foreach($params as $field => $value){
		if(ctype_digit($value)){
			$st->bindValue(':' . $field, $value, PDO::PARAM_INT);
		}else {
			$st->bindValue(':' . $field, $value, PDO::PARAM_STR);
		}
	}
	try {
		$st->execute();
		$st = NULL;

		return true;
	}catch (PDOException $e){
		var_dump($e->errorInfo);
	}
}


/**
 * @param $sp
 * @param array $params
 * @param array $bindings
 * @return array
 * @throws \Mf_Core\Upload\Exception
 */
public function bindFetch($queryStr, Array $params, Array $bindings){

	$st = $this->_driver->prepare($queryStr);

	# bind parameters
	if(!empty($params)){
		foreach($params as $key => $value){
			if(ctype_digit($value)){
				$st->bindValue(':' . $key, $value, PDO::PARAM_INT);
			}else {
				$st->bindValue(':' . $key, $value, PDO::PARAM_STR);
			}
		}
	}

	$st->execute();


	# for each column binding, bind column
	foreach($bindings as $binding){
		$st->bindColumn($binding, $$binding);
	}

	$st->fetch(PDO::FETCH_ASSOC);

	# build response
	$response = array();

	foreach($bindings as $binding){
		$response[$binding] = $$binding;
	}

	return $response;

}

public function subscribeUserForMomentNotifications($userId, $momentId)
{

		$st = $this->_driver->prepare("INSERT IGNORE INTO MomentSubscribers (SubscriberId, MomentId) values (:userId, :momentId) ");
		$st->bindValue(':userId', $userId, PDO::PARAM_STR);
		$st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
		return ($st->execute()) ? true : false;

}

public function storeInteraction($userId, $momentId, $type)
{

			$st = $this->_driver->prepare("INSERT  INTO Interaction (UserId, MomentId, InteractionType, Time) values (:userId, :momentId, :type, :time) ");
			$st->bindValue(':userId', $userId, PDO::PARAM_STR);
			$st->bindValue(':momentId', $momentId, PDO::PARAM_STR);
			$st->bindValue(':type', $type, PDO::PARAM_INT);
			$st->bindValue(':time', time(), PDO::PARAM_STR);
			if($st->execute()){ return $this->_driver->lastInsertId();  } else { return "error"; }
}

public function storeInteractionNotification($notificationOwnerId, $notificationInititorId,  $contentcreatorId, $interactionId)
{
	$st = $this->_driver->prepare("INSERT  INTO InteractionNotification (NotificationOwnerId, NotificationInitiatorId, ContentCreatorId, InteractionId) values
	(:notificationOwnerId, :notificationInitiatorId, :contentCreatorId, :interactionId) ");
	$st->bindValue(':notificationOwnerId', $notificationOwnerId, PDO::PARAM_STR);
	$st->bindValue(':notificationInitiatorId', $notificationInititorId, PDO::PARAM_STR);
	$st->bindValue(':contentCreatorId', $contentcreatorId, PDO::PARAM_STR);
	$st->bindValue(':interactionId', $interactionId, PDO::PARAM_INT);
  if($st->execute()){ return $this->_driver->lastInsertId();  } else { return "error"; }

}


}
