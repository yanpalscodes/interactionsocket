<?php
/**
 *
 */
use Mf_Core\Database\Database;
use Mf_Core\Database\Driver\Pdo;
use Mf_Core\Config\Config;

/**
 * Base Database
 */
class DatabaseMain extends Database
{
	public function __construct()
	{
		$config = Config::getInstance();
		$dbConfig = $config->get('maindatabase');

		try {
			$driver = new Pdo($dbConfig->host, $dbConfig->user, $dbConfig->pass, $dbConfig->name);
			parent::__construct('wokondb', $driver);
		} catch (Exception $ex) {
			exit('Database connection error');
		}
	}

	public function fetchUserBio($userId)
	{
		$query = $this->_driver->prepare('CALL sp_FetchUserDetailsByUserId(:UserId)');
		$query->bindValue(':UserId', $userId, PDO::PARAM_STR);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC);
		unset($query);

		return $result;
	}

	/**
	 * Fetch Users pals
	 */
	public function fetchUserPals($userId, $limit=20, $offset=0)
	{
		

		/* Fetch user pals by looking in the UserPals table */
		$sql = "SELECT UserPals.Id as UserPalsId, UserBiodata.UserId, UserBiodata.FirstName, UserBiodata.LastName, UserBiodata.Avatar,\n"
			."UserAuthentication.Username, UserAuthentication.OnlineStatus\n"
			."FROM UserPals\n"
			."INNER JOIN UserBiodata\n"
			."ON UserPals.PalUserId = UserBiodata.UserId\n"
			."INNER JOIN UserAuthentication\n"
			."ON UserPals.PalUserId = UserAuthentication.UserId\n"
			."WHERE UserPals.UserId = :UserIdLeft\n"
			."UNION ALL\n"
			."SELECT UserPals.Id as UserPalsId, UserBiodata.UserId, UserBiodata.FirstName, UserBiodata.LastName, UserBiodata.Avatar,\n"
			."UserAuthentication.Username, UserAuthentication.OnlineStatus\n"
			."FROM UserPals\n"
			."INNER JOIN UserBiodata\n"
			."ON UserPals.UserId = UserBiodata.UserId\n"
			."INNER JOIN UserAuthentication\n"
			."ON UserPals.UserId = UserAuthentication.UserId\n"
			."WHERE UserPals.PalUserId = :UserIdRight\n"
			."LIMIT :offset, :limit";


		// @TODO Fetch all details of each pal
		$query = $this->_driver->prepare($sql);
		$query->bindParam(':UserIdLeft',$userId,PDO::PARAM_STR);
		$query->bindParam(':UserIdRight',$userId,PDO::PARAM_STR);
		$query->bindValue(':offset',$offset,PDO::PARAM_INT);
		$query->bindValue(':limit',$limit,PDO::PARAM_INT);
		$query->execute();
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		unset($query);
		return $result;
	}

	/**
	 * Fetches friends of user A who are also friends of user B
	 * sp_CountMutualPals
	 */
	/*
	public function fetchMutualPals($userId, $palId)
	{

	}
	*/

}
