<?php
// Direct access check
defined('_PUBLIC') || exit;
defined("DIR_ROOT_APP") || define("DIR_ROOT_APP", __DIR__);

//
// Register autoloaders
//
require_once DIR_ROOT_APP . '/library/Mf_Core/Autoloader.php';
Mf_Core\Autoloader::register();

// Load Config
use \Mf_Core\Config\Config;
Config::getInstance(DIR_ROOT_APP.'/app.config.php');

// Load Registry
use \Mf_Core\Registry;
Registry::getInstance();

// Load Databases
require_once DIR_ROOT_APP . '/database/Moments.php';
Registry::getInstance()->set('MomentDb', new DatabaseMoments());


// Load Databases
require_once DIR_ROOT_APP . '/database/Comments.php';
Registry::getInstance()->set('CommentDb', new DatabaseComments());



require_once DIR_ROOT_APP . '/database/DatabaseMain.php';
Registry::getInstance()->set('MainDb', new DatabaseMain());
require_once DIR_ROOT_APP . '/database/User.php';
Registry::getInstance()->set('UserDb', new DatabaseUser());


require_once DIR_ROOT_APP . '/database/User.php';
Registry::getInstance()->set('UserDb', new DatabaseUser());


require_once DIR_ROOT_APP . '/database/Notifications.php';
Registry::getInstance()->set('NotificationDb', new DatabaseNotifications());




//require smartkon libraies here




 require_once DIR_ROOT_APP . '/library/Mainsocket/GeneralFunctions.php';

 require_once DIR_ROOT_APP . '/library/Mainsocket/YanPalUser.php';

 require_once DIR_ROOT_APP . '/library/Mainsocket/UserHelper.php';

 require_once DIR_ROOT_APP . '/library/Mainsocket/Moment.php';

require_once DIR_ROOT_APP . '/library/Mainsocket/Comment.php';
