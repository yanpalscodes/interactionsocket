<?php
// Deny direct access
//defined('_PUBLIC') || exit('<font color="#ff0000">#ACCESS_DENIED<font color="red">');

/**
 * NOTE: Configuration files must be kept outside your
 * www/ folder and ignore by git
 *
 * How to configure your application
 * - Create $config array with keys
 * - If you need more than one config file, it's
 *   easier to just include them
 */

// Perform check
$config = isset($config) ? $config : array();

//
// @GROUP Database
//
//$config['host.*'] = "glo";
$config['maindatabase.prefix'] = 'default_';
$config['maindatabase.user'] = 'root';
$config['maindatabase.name'] = 'wokondb';
$config['maindatabase.port'] = '3306';

$config['maindatabase.host'] = '159.65.22.6';
//$config['maindatabase.host'] = 'localhost';

//$config['maindatabase.pass'] = 'linuxguy';
$config['maindatabase.pass'] = 'Smartkon@0.1db';




//
//@GROUP Uploads
//
$uploadUrl = 'http://159.65.22.6';
$config['uploads.videoUrl'] =  $uploadUrl.'/yuploader/public/uploads/videos/';
$config['uploads.avatarUrl'] = $uploadUrl.'/yuploader/public/uploads/images/avatar/';
$config['uploads.board'] = $uploadUrl.'/yuploader/public/uploads/images/board/';
$config['uploads.mansonry'] = $uploadUrl.'/yuploader/public/uploads/images/mansonry/';
$config['uploads.coverPhotoUrl'] = $uploadUrl.'/yuploader/public/uploads/images/coverPhotos/';
$config['uploads.originalPhotoUrl'] = $uploadUrl.'/yuploader/public/uploads/images/original/';
$config['uploads.posters'] = $uploadUrl.'/yuploader/public/uploads/images/posters/';
$config['uploads.loopUrl'] = $uploadUrl.'/yuploader/public/uploads/videos/loop/';


//
//@GROUP JWT secret
//
$config['jwt.secret'] = 'iamalegend@0.1';
