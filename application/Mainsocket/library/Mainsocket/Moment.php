<?php
// Direct access check


namespace Mf\Mainsocket\library\Mainsocket;
use Mf\Mainsocket\library\Mainsocket\DatabaseMoments;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\User;





class Moment{

	public $momentId;
	public $content;
	public $media = NULL;
	public $time;
	public $userId;
	public $isOwner;
	public $commentCount = 0;
	public $originalMomentId;
	public $hasRebroadcasted;
	public $rebroadcastCount = 0;
	public $rebroadcastDetails;
	public $userInfo;
	public $location;
	public $priority;
	private $dbMoments;
	public $notificationEnabled;
	public $loveCount;
	public $hasRebroadcastDetails = "false";
	public $loved = "false";




	public function __construct(Array $options, $loggedInUserId='', $fetchComment)
	{
		$this->dbMoments = Registry::getInstance()->get('MomentDb');
		$this->originalMomentId = isset($options['OriginalMomentId']) ? $options['OriginalMomentId'] : NULL;
		if(strlen($this->originalMomentId) > 10){//check if the post is a rebroadcast
			$this->hasRebroadcastDetails = "true";
			$rebroadcastArray = $options;
			$options = $this->dbMoments->getMomentByMomentId($this->originalMomentId);
			$this->prepareRebroadcastObject($rebroadcastArray, $options);
		}
		$this->momentId = isset($options['MomentId']) ? $options['MomentId'] : NULL;
		$this->time = isset($options['Time']) ? $options['Time'] : NULL;
		$this->userId = isset($options['UserId']) ? $options['UserId'] : NULL;
		$this->isOwner = ($loggedInUserId == $this->userId) ? "true" : "false";
		$this->content = isset($options['Content']) ? $options['Content'] : '';
		$this->priority =  isset($options['priority']) ? $options['priority'] : '';
		$this->location = ($options['Location'] != null)?  json_decode($options['Location']): '';
		$this->countRebroadcast($this->momentId);
		$this->countMomentComments($this->momentId);
		$this->userInfo = $this->_getUser($this->userId);
		$this->arrangeMedia($this->momentId);
		$this->checkIfRebroadcasted($this->momentId, $loggedInUserId);
		$this->loveCount = $this->loveCounts($this->momentId);
		$this->checkIfSubscriptionEnabled();
		$this->loved = $this->checkIfLoved($this->momentId, $loggedInUserId);

	}

	/*
	* Returns instance variables if they exist
	*/
	public function get($key){
		$newKey = (strpos($key, '_') === false) ? '_'.$key : $key;
		if(property_exists(__CLASS__, $newKey)){
			return $this->{$newKey};
		}
		return false;
	}




private function getComments($momentId, $loggedInUserId)
{
	$db = new DatabaseComments();
	$commentArray = $db->getCommentsByMomentId($momentId, $loggedInUserId, 5);//fetch  5 comments from db
	if(count($commentArray) > 0){//if there are commments
		$pageStart = $commentArray[0]["Id"];
		$pageEnd = $commentArray[count($commentArray)-1]["Id"];
	}
	else{//if there is no comment
		$pageStart = null;
		$pageEnd = null;
	}
	$responseObj  = array();
	foreach ($commentArray as $one) {
		$comment  = new Comment($one);
		$responseObj[] = $comment;
		}
	 $this->commentObject = array("list"=>array_reverse($responseObj),
	 "commentsReturned"=>count($commentArray), "pageStart"=>$pageStart, "pageEnd"=>$pageEnd);
 		$this->commentCount = $db->countMomentComments($momentId);
		$this->commentersCount = $db->countMomentCommenters($momentId);

}

	private function checkIfRebroadcasted($momentId, $loggedInUserId)
	{

		$this->hasRebroadcasted = ($this->dbMoments->checkIfRebroadcasted($momentId, $loggedInUserId) == true)? "true" : "false";

	}

	private function checkIfSubscriptionEnabled()
	{
		$this->notificationEnabled = ($this->dbMoments->checkIfSubscriptionEnabled($this->userId, $this->momentId) == true)? "true" : "false";
	}


	private function _getUser($userId){
		$db = Registry::getInstance()->get('UserDb');
		$userInfo = $db->fetchUser($userId);
		$userInfo =  get_object_vars($userInfo);
		if(is_array($userInfo)){
			return new YanPalUser($userInfo);
		}
		return;
	}




	private function prepareRebroadcastObject($rebroadcastArray , $option)
	{

		$rebroadcasterObject = $this->_getUser($rebroadcastArray["UserId"]);
		$time = $rebroadcastArray["Time"];
		$ownerRebroadcasted = ($rebroadcastArray["UserId"] == $option["UserId"])? "true" : "false";
		$this->rebroadcastDetails = array("rebroadcaster"=>$rebroadcasterObject, "time"=>$time, "ownerRebroadcasted"=>$ownerRebroadcasted, "momentId"=>$rebroadcastArray["MomentId"] );

 }//ends getrebdetails





	private function  countRebroadcast($momentId)
	{
		$this->rebroadcastCount = $this->dbMoments->countNumberOfRebroadcastForAMoment($momentId);
	}



	private function arrangeMedia($momentId)
	{
			$mediaDetails = $this->dbMoments->getMediaByMomentId($momentId);
			$this->continueToArrangeMedia($mediaDetails);
	}

	private function continueToArrangeMedia($mediaDetails)
	{

		$config = Config::getInstance();
		$uploadsConfig = $config->get('uploads');
		$media = array();
		foreach($mediaDetails as $one){
			$oneMedia = array();
		  $oneMedia["type"] = $this->typeSwitcher($one["Type"]);
			$oneMedia["mediaId"] = $one["MediaId"];
			//now append url from config
			$oneMedia["url"] = ($oneMedia["type"] == "image")? $uploadsConfig->originalPhotoUrl.$one["MediaName"] :
			$uploadsConfig->videoUrl.$this->getVideoFolderNameAnDVideoName($one["MediaName"]) ;

			//now compose url for the dash file
			$oneMedia["mpdUrl"] = ($oneMedia["type"] == "video")?  $uploadsConfig->videoUrl.$this->getDashUrl($one["MediaName"]): NULL;

			//now compose url for the segment
			$oneMedia["segmentInitUrl"] = ($oneMedia["type"] == "video")?  $uploadsConfig->videoUrl.$this->getSegmentUrl($one["MediaName"]): NULL;


			$oneMedia["loopUrl"] = ($oneMedia["type"] == "video")?  $this->getLoopUrl($one) : NULL;
			$oneMedia["poster"] =  $uploadsConfig->posters.$one["Poster"];
			$media[] = $oneMedia;
		}
		$this->media = $media;
	}





	public function getLoopUrl($one)
	{
		$config = Config::getInstance();
		$uploadsConfig = $config->get('uploads');

				$file = $uploadsConfig->loopUrl.$one["MediaName"];
		$file_headers = @get_headers($file);
		if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
		    return NULL;
		}
		else {
		    	return $uploadsConfig->loopUrl.$one["MediaName"];
		}

	}




	private function getVideoFolderNameAnDVideoName($videoFileName)
	{
		$foldername = explode(".",$videoFileName);
		return $foldername[0]."/".$videoFileName;
	}

  private function getDashUrl($fileName)
	{
		$foldername = explode(".",$fileName);
		return $foldername[0]."/". $foldername[0]  ."_encoded_dash.mpd";
	}

	private function getSegmentUrl($fileName)
	{
		$foldername = explode(".",$fileName);
		return $foldername[0]."/"."segment_init.mp4";
	}

	private function typeSwitcher($type)
	{
		if($type == 1){
			return "image";
		}
		elseif($type == 2)
		{
				return "video";
		}
	}

	private function countMomentComments($momentId)
	{
		$this->commentCount =  $this->dbMoments->countMomentComments($momentId);
	}

	private function loveCounts($momentId)
	{
		return $this->dbMoments->loveCounts($momentId);
	}

	private function checkIfLoved($momentId, $loggedInUserId)
	{
	  return ($this->dbMoments->checkIfLoved($momentId,$loggedInUserId) == true)? "true" : "false";
	}




}
