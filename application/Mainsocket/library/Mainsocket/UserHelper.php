<?php
/**
 *
 */
namespace Mf\Mainsocket\library\Mainsocket;

/**
 *
 */
class UserHelper
{
	//
	public $id;

	//
	

	/**
	 *
	 */
	public function __construct($id, array $properties)
	{
		$this->id = $id;
		$this->setProperties($properties);
	}

	/**
	 *
	 */
	public function setProperties(array $properties)
	{
		foreach($properties as $property => $value)
		{
			$this->setProperty($property, $value);
		}

		return true;
	}

	/**
	 *
	 */
	public function setProperty($property, $value)
	{
		$this->{$property} = $value;
	}
}