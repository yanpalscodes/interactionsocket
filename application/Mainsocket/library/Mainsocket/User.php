<?php
namespace Mf\Mainsocket\library\Mainsocket;
//use Mf_Core\User;
use Mf_Core\Database;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\User;



class YanPalUser
{
	public $userId;
	public $firstName;
	public $lastName;
	public $gender;
	public $day;
	public $month;
	public $year;
	public $birthday;
	public $email;
	public $phone;
	public $settings;
	public $onlineStatus;
	public $accountStatus;
	public $device;
	public $username;
	public $accountType;
	public $dateJoined;
	public $website;
	public $urlName;
	public $cityId;
	public $stateId;
	public $countryId;
	public $bioInfo;
	public $city;
	public $state;
	public $country;
	public $userCoverMedia;
	public $avatar;
	public $userCoverMediaType;
	public $posterUrl;


	/**
	 *
	 */
	protected static $_instance;

	/**
	 * Creates instance of User
	 */
	public function __construct($options)
	{
		$this->UserDb = Registry::getInstance()->get('UserDb');
		$options = (is_bool($options))? array(): $options;
		$this->userId = isset($options['UserId']) ? $options['UserId'] : 0;
		$this->firstName = isset($options['FirstName']) ? ucfirst($options['FirstName']) : NULL;
		$this->lastName = isset($options['LastName']) ? ucfirst($options['LastName']) : NULL;
		$this->gender = isset($options['Gender']) ? $options['Gender'] : NULL;
		$this->cityId = isset($options['CityId']) ? $options['CityId'] : NULL;
		$this->email = isset($options['Email']) ? $options['Email'] : NULL;
		$this->phone = isset($options['Phone']) ? $options['Phone'] : NULL;
		$this->settings = isset($options['Settings']) ? $options['Settings'] : NULL;
		$this->accountStatus = isset($options['AccountStatus']) ? $options['AccountStatus'] : NULL;
		$this->username = isset($options['Username']) ? $options['Username'] : NULL;
		$this->accountType = isset($options['AccountType']) ? $options['AccountType'] : NULL;
		$this->dateJoined = isset($options['DateJoined']) ? $options['DateJoined'] : NULL;
		$this->stateId = isset($options['StateId']) ? $options['StateId'] : NULL;
		$this->countryId = isset($options['CountryId']) ? $options['CountryId'] : NULL;
		$this->cityId = isset($options['CityId']) ? $options['CityId'] : NULL;
		$this->website = isset($options['Website']) ? $options['Website'] : NULL;
		$this->urlName = isset($options['Urlname']) ? $options['Urlname'] : NULL;//this will be used for the person profile page
		$this->bioInfo = isset($options['BioInfo']) ? $options['BioInfo'] : NULL;
		$this->day = isset($options['DayOfBirth']) ? $options['DayOfBirth'] : NULL;
		$this->month = isset($options['MonthOfBirth']) ? $options['MonthOfBirth'] : NULL;
		$this->year = isset($options['YearOfBirth']) ? $options['YearOfBirth'] : NULL;
		$this->isVerified = isset($options['IsVerified']) ? $options['IsVerified'] : NULL;
		$this->registrationStep = isset($options['RegistrationStep']) ? $options['RegistrationStep'] : NULL;
		$this->birthday = $this->arrangeBirthDay();
		$this->_arrangeUrls($options);

		if($this->cityId !== NULL){
			$this->_fetchUserCityName($this->cityId);
		}
		if($this->stateId !== NULL){
			$this->_fetchStateName($this->stateId);
		}
		if($this->countryId !== NULL){
			$this->_fetchUserCountryName($this->countryId);
		}


	}

	/**
	 * Gets instance of User using application
	 *
	 * @override
	 */
	public static function getInstance($id = 0)
	{
		if(!self::$_instance) {
			if(!$id)
			{
				@self::$_instance = new self( array("id"=>$id) );
			}
			else {
				// @todo Fetch user details from DB
				$db = new DatabaseUser;
				$options = $db->fetchUser($id);
				@self::$_instance = new self($options);
			}
		}

		return self::$_instance;
	}

	/**
	 * Fetches user details from database
	 */
	protected static function findUserById($id, Database $db = null)
	{
		 $result = $this->_db->fetchUserById($id);
		 return $result;
	}





	private function _fetchUserCityName($cityId)
	{

			$this->city = $this->UserDb->getCityNameByCityId($cityId);//get city Name
	}

	private function _fetchStateName($stateId)
	{

			$this->state = $this->UserDb->getStateNameByStateId($stateId);//get State Name
	}

	private function _fetchUserCountryName($countryId)
	{

			$this->country = $this->UserDb->getCountryNameByCountryId($countryId);//get country  Name
	}

	private function  _arrangeUrls($options)
	{
		$config = Config::getInstance();
		$uploadsConfig = $config->get('uploads');


		$userMediaUrl = $this->setUserMediaUrl($uploadsConfig, $options['CoverMediaType']);

		$this->avatar = isset($options['Avatar']) ?
		$uploadsConfig->avatarUrl.($options['Avatar']) : NULL;


		if (isset($options['CoverMedia'])) {
			 $this->userCoverMedia = $this->userCoverMediaType == 'video' ? $userMediaUrl . ((explode(".", $options['CoverMedia'])[0]) . "/" . ($options['CoverMedia'])) :
			  $userMediaUrl . ($options['CoverMedia']); } else {  $this->userCoverMedia = NULL;
		 }

	}

	/**
	* This method assigns a url to the userCoverMedia depending on the userCoverMediaType. 1 for images
	* 2 for videos
	*/
private function setUserMediaUrl($uploadsConfig, $coverMediaType)
{
	switch ($coverMediaType)
	{
		case 1:
				$this->userCoverMediaType = 'photo' ;
			return $uploadsConfig->coverPhotoUrl;
			break;
		case 2:
			$this->userCoverMediaType = 'video' ;
			$this->posterUrl = $uploadsConfig->originalPhotoUrl . "posters/" . (explode(".", $coverMedia)[0]) .".jpg";
			return $uploadsConfig->videoUrl;
			break;
		default://do nothing for now
		}
}

private function arrangeBirthDay()
{

	return $this->day. "-". $this->month. "-".$this->year;
}


}
