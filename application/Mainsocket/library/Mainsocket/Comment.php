<?php
namespace Mf\Mainsocket\library\Mainsocket;
use  Mf\Mainsocket\library\Mainsocket\YanPalUser;
use \Mf_Core\Registry;
use Mf_Core\Config\Config;
/*
 *
 *
 *
*/

Class Comment
{

	public $commentId;
	public $momentId;
	public $content;
	public $time;
	public $userId;
	public $trashed;
	public $userInfo;
	public $media;
	private $UserDb;
	public $replyCount;
	public $mediaId;
	public $originalCommentId;

	//construct method collects a linear array of a comment, splits the array to get the properties of the comment
	public function __construct($options)
	{
		$this->UserDb = Registry::getInstance()->get('UserDb');
		$this->commentId = isset($options['CommentId']) ? $options['CommentId'] : '';
		$this->momentId = isset($options['MomentId']) ? $options['MomentId'] : '';
		$this->content = isset($options['Content']) ? $options['Content'] : '';
		$this->mediaId = isset($options['MediaId']) ? $options['MediaId'] : '';
		$this->userId = isset($options['UserId']) ? $options['UserId'] : '';
		$this->time = isset($options['Time']) ? $options['Time'] : '';
		$this->trashed = isset($options['Trashed']) ? $options['Trashed'] : 0;
		$this->originalCommentId = isset($options['OriginalCommentId']) ? $options['OriginalCommentId'] : '';
		$this->userInfo = $this->_getUser($this->userId);
		$this->arrangeMediaIfItExist($this->mediaId);
		$this->countReply();
	}


	//this method gets the value from a property of a class
	public function get($key)
	{
		$newKey = (strpos($key, '_') === false) ? '_'.$key : $key;
		if(property_exists(__CLASS__, $newKey)){
			return $this->{$newKey};
		}
		return false;
	}


	private function arrangeMediaIfItExist($mediaId)
	{
		$db = Registry::getInstance()->get('CommentDb');
		if($mediaId != ''){

			 $one = $db->getMediaDetailsByMediaId($mediaId);
			 $config = Config::getInstance();
			 $uploadsConfig = $config->get('uploads');

				$oneMedia["type"] = $this->typeSwitcher($one["Type"]);
				$oneMedia["mediaId"] = $one["MediaId"];
				//now append url from config
				$oneMedia["url"] = ($oneMedia["type"] == "image")? $uploadsConfig->originalPhotoUrl.$one["MediaName"] :
				$uploadsConfig->videoUrl.$this->getVideoFolderNameAnDVideoName($one["MediaName"]) ;

				//now compose url for the dash file
				$oneMedia["mpdUrl"] = ($oneMedia["type"] == "video")?  $uploadsConfig->videoUrl.$this->getDashUrl($one["MediaName"]): NULL;

				//now compose url for the segment
				$oneMedia["segmentInitUrl"] = ($oneMedia["type"] == "video")?  $uploadsConfig->videoUrl.$this->getSegmentUrl($one["MediaName"]): NULL;

				$oneMedia["loopUrl"] = ($oneMedia["type"] == "video")?  $this->getLoopUrl($one) : NULL;
				$oneMedia["poster"] =  $uploadsConfig->posters.$one["Poster"];

			$this->media = $oneMedia;

		}
		else{
			$this->media = NULL;
		}
	}


	private function countReply()
	{
		$db = Registry::getInstance()->get('CommentDb');
		$this->replyCount = $db->countCommentsReply($this->commentId);
	}




	private function _getUser(){
		$db = Registry::getInstance()->get('UserDb');
		$userInfo = $db->fetchUser($this->userId);
		$userInfo =  get_object_vars($userInfo);
		if(is_array($userInfo)){
			return new YanPalUser($userInfo);
		}
		return;
	}

	public function getLoopUrl($one)
	{
		$config = Config::getInstance();
		$uploadsConfig = $config->get('uploads');

				$file = $uploadsConfig->loopUrl.$one["MediaName"];
		$file_headers = @get_headers($file);
		if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
				return NULL;
		}
		else {
					return $uploadsConfig->loopUrl.$one["MediaName"];
		}

	}




	private function getVideoFolderNameAnDVideoName($videoFileName)
	{
		$foldername = explode(".",$videoFileName);
		return $foldername[0]."/".$videoFileName;
	}

	private function getDashUrl($fileName)
	{
		$foldername = explode(".",$fileName);
		return $foldername[0]."/". $foldername[0]  ."_encoded_dash.mpd";
	}

	private function getSegmentUrl($fileName)
	{
		$foldername = explode(".",$fileName);
		return $foldername[0]."/"."segment_init.mp4";
	}

	private function typeSwitcher($type)
	{
		if($type == 1){
			return "image";
		}
		elseif($type == 2)
		{
				return "video";
		}
	}




}
?>
