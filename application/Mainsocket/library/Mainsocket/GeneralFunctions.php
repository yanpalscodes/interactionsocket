<?php
namespace Mf\Mainsocket\library\Mainsocket;

class GeneralFunctions
{


		public function IdGenerator()
		{
			$word = "abdeghijklmnqstuwxyz012456789";
			$randomKey = str_shuffle($word);
			$key = substr($randomKey,0,9);
			$id = uniqid('',true).$key;
			return $id;

		}


		public function OrdinaryIdGen()
		{
			return uniqid("", true);
		}


		public function sanitizeInput($input)
		{
			$input = filter_var($input, FILTER_SANITIZE_STRING);
			$input = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $input);
			return $input;
		}

		public function NotificationType( $type )
		{
		    switch ($type) {
	            case 'rebroadcast':
	                return "1";
	                break;
	            case 'comment':
	                return "2";
	                break;
	            case 'follow':
	                return "3";
	                break;
	            case 'dm':
	                return "4";
	                break;
	            case 'livestream':
	                return "5";
	                break;
	            case 'moment':
	                return "6";
	                break;
	            default:
	                echo "0";
	        }
	    }


	}
