<?php
/**
 *
 */
namespace Mf_Core\Lang; 

use \InvalidArgumentException;
use Mf_Core\Lang\TypeAbstract;

/**
 * Creates object of stdClass
 */
class _Object extends TypeAbstract
{
	/**
	 * Converts var from other datatype to stdClass obj
	 *
	 * @param 	$var 	mixed
	 */
	public function __construct($var)
	{
		$this->_value = (object) $var;
	}
	
	/**
	 * Returns object property
	 *
	 * @param 	$key
	 */
	public function getProperty($key)
	{
		if(property_exists($this->getValue(), $key))
		{
			return $this->getValue()->{$key};
		}
		
		return null;
	}
	
	/**
	 * Sets object property
	 *
	 * @param 	$key
	 */
	public function setProperty($key, $value)
	{
		$obj = $this->getValue();
		if(property_exists($this->getValue(), $key))
		{
			$current = $obj->{$key};
			if($current !== $value) $obj->{$key} = $value;
			return true;
		}
		
		return false;
	}
}