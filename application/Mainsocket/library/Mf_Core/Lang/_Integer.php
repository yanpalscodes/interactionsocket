<?php
/**
 *
 */
namespace Mf_Core\Lang; 

use \InvalidArgumentException;
use \Exception;
use Mf_Core\Lang\TypeAbstract;

/**
 * Integer datatype wrapper
 * Performs conversion to base 10 integer.
 *
 * NOTE: Should not be used on objects and arrays.
 */
class _Integer extends TypeAbstract
{
	const BASE_NONE = 0;
	const BASE_BIN = 2;
	const BASE_OCT = 8;
	const BASE_DEC = 10;
	const BASE_HEX = 16;
	
	/**
	 * Performs conversion to base 10 integer.
	 * If $base param is given, the value of var will be converted from given
	 * base to base 10.
	 *
	 * intval(0xff) // 255. Base is determined by the format of $var since $var is integer
	 * intval('0xff', 0) // 255. Base is determined by the format of $var
	 * intval('0xff', 16) // 255. Converts $var from base 16 to base 10
	 * intval('0xff') // 0. Uses default base 10
	 *
	 * @param 	$var 	mixed
	 * @param 	$base 	int 	Number base of $var. Default is base 10
	 */
	public function __construct($var, $base=10)
	{
		$invalid = array('object', 'array');
		if(in_array(gettype($var), $invalid))
			throw new InvalidArgumentException('unsupported');
		
		if(!function_exists('intval')) {
			throw new Exception('PHP function "intval" does not exists');
		}

		$this->_value = intval( $var, $base );
		/*
		else {
			$this->_value = $this->_intval($var, $base);
		}
		*/
	}

	/**
	 * Converts value to binary integer
	 */
	public function getBin()
	{
		//decbin
		return sprintf("%b", $this->_value);
	}
	
	/**
	 * Converts value to binary integer
	 */
	public function getHex()
	{
		//dechex
		return sprintf("%x", $this->_value);
	}
	
	/**
	 * Converts value to binary integer
	 */
	public function getOct()
	{
		// decoct
		return sprintf("%o", $this->_value);
	}
	
	/**
	 * Lazy intval conversion
	 */
	private function _intval($var, $base = 10)
	{
		if(is_string($var) && $base == 0)
		{
			// Find prefix
			if(preg_match('/^0x?/i', $var, $matches))
			{
				$base = ($matches[0] === '0x') ? 16 :
					(($matches[0] === '0') ? 8 : $base);
			}
		}
		
		$formats = array(2=>'%b', 8=>'%o', 10=>'%d', 16=>'%x');
		if(!array_key_exists($base, $formats))
			$base = 10;
		
		$format = $formats[$base];
		//var_dump($format);
		
		return sprintf($format, $var);
	}
}