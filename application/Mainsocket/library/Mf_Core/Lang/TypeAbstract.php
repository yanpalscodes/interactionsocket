<?php
/**
 *
 */
namespace Mf_Core\Lang;

use \Exception;

/**
 *
 */
abstract class TypeAbstract
{
	// $var mixed
	protected $_value;

	/**
	 *
	 */
	public function getValue()
	{
		return $this->_value;
	}

	/**
	 * Objects that can be converted to string must
	 * override this method
	 * 
	 * NOTE: Object, Array cannot be converted to string.
	 */
	public function toString()
	{
		throw new Exception('Cannot convert object to string');
	}
	
	/**
	 * Converts object to JSON string
	 */
	public function toJsonString()
	{
		$json = json_encode($this->getValue());
		if(json_last_error() !== JSON_ERROR_NONE)
		{
			throw new Exception(json_last_error_msg());
		}
		
		return $json;
	}
}