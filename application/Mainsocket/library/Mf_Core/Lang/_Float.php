<?php
/**
 *
 */
namespace Mf_Core\Lang; 

use \InvalidArgumentException;
use Mf_Core\Lang\TypeAbstract;

/**
 * Float datatype wrapper
 * Performs conversion to float
 *
 * NOTE: Should not be used on objects and arrays
 */
class _Float extends TypeAbstract
{
	/**
	 *
	 */
	public function __construct($var)
	{
		if(function_exists('floatval')) {
			$this->_value = floatval($var);
		}
		else {
			$this->_value = (float) $var;
		}
	}
	
	/**
	 *
	 */
	public function toPrecision($zeros)
	{
	}
}