<?php
/**
 *
 */
namespace Mf_Core\Lang; 

use \InvalidArgumentException;
use Mf_Core\Lang\TypeAbstract;

/**
 * Array datatype
 * 
 * Converts scalar values to an array
 *
 * Scalar variables are those containing an integer, float, string or boolean.
 * Types array, object and resource are not scalar.
 *
 * Recommended: Store values of same type in an array to reduce
 * chances for mistakes and bugs in your application
 *
 * Methods:
 * - getCount(void)
 * - getSize(void)
 * - getValue(void)
 * - indexOf(scalar)
 * - merge(array)
 * - pop(void | int)
 * - push(scalar[, ...])
 * - shift(void | int) // Remove item(s) at beginning
 * - unshift(scalar[, ...]) // Add item(s) at beginning
 * - slice
 * - splice
 * - sort
 * - replace($search, $replacement)
 * - search
 * - reverse
 * - each($iterator);
 * - toString
 */
class _Array extends TypeAbstract
{
	/**
	 * Creates an array from supplied arguments
	 * new _Array(1, 2, 3); // => array(1, 2, 3)
	 *
	 * @param 	void 	Optional arguments are passed
	 */
	public function __construct()
	{
		$args = func_get_args();
		$array = array();
		foreach($args as $var)
		{
			$array[] = (is_object($var) && get_class($var) === __CLASS__) ?
					$var->getValue() : $var;
		}
		$this->_value = $array;
	}
	
	/**
	 *
	 */
	public function getCount()
	{
		return count($this->getValue());
	}
	
	/**
	 * Alias of getCount()
	 */
	public function getSize()
	{
		return $this->getCount();
	}
	
	
	/**
	 * Iterates over each element of the array
	 *
	 * @param 	$callback 	callback 	Typically, callback takes two optional parameters.
	 * 									The first being the value, and the second key/index of element.
	 */
	public function each($callback)
	{
		if(!is_callable($callback))
		{
			throw new InvalidArgumentException('Argument 1 must be a callback');
		}
		
		foreach($this->_value as $index => $value)
		{
			call_user_func($callback, $value, $index);
		}
	}
	
	/**
	 * Merges another array value
	 */
	public function merge(_Array $var)
	{
		$array = $var->getValue();
		
		$this->_value = array_merge($this->_value, $array);
		return $this->_value;
	}
}