<?php
/**
 *
 */
namespace Mf_Core\Application;

use \Exception;
use \Mf_Core\Application\Application;

/**
 * Base class for Web Applications
 *
 * @package
 */
abstract class Web extends Application
{
	/**
	 * Application environment
	 *
	 * @var String
	 */
	protected $_env;

	/**
	 * Server physical root
	 */
	protected $_docRoot;

	/**
	 * ApplicationWeb instance
	 */
	protected static $_instance;
	
	/**
	 *
	 */
	public function __construct($path)
	{
		// Parent
		parent::__construct($path);

		// Doan run webapps on cli
		if($this->getSapi() === 'cli')
		{
			throw new Exception("INVALD_APPLICATION_ENVIRONMENT", 500);
		}

		// Define application environment
		$this->setEnv( PHP_SAPI == 'cli-server'
			|| (isset($_SERVER['APPLICATION_ENV'])
				&& $_SERVER['APPLICATION_ENV'] == 'development')
			? 'development' : 'production');

		// Set document root
		$this->_docRoot = $_SERVER['DOCUMENT_ROOT'];
	}

	/**
	 * Creates a returns a client web application
	 */
	public static function getInstance($path = '', $namespace = '', $client='Web')
	{
		if(self::$_instance)
		{
			return self::$_instance;
		}

		$appDir = $path . '/application/Application.php';
		$appClass = ucfirst($namespace) . 'Application';

		if(!file_exists($appDir))
		{
			throw new Exception("APPLICATION_EXCEPTION", 500);
		}

		require_once $appDir;
		self::$_instance = new $appClass($path);

		return self::$_instance;
	}

	/**
	 * Sets application environment
	 */
	public function setEnv( $environment )
	{
		$this->_env = ($environment == 'development') ? $environment : 'production';
	}

	/**
	 * Returns application environment
	 */
	public function getEnv() {
		return $this->_env;
	}

	/**
	 * Initialise application
	 *
	 * @see Application
	 */
	public function initialise()
	{
		// @leave blank
	}

	/**
	 * @see Application
	 */
	public function execute()
	{
		// @leave blank
	}
}