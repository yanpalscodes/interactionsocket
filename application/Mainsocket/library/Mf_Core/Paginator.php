<?php
/**
 *
 */
namespace Mf_Core;

/**
 *
 */
class Paginator
{
	// Data being paginated
	// @var 	array
	protected $_data;
	
	// Initial offset
	protected $_offset;
	
	// Items to display per page
	protected $_limit;
	
	// Size of items
	protected $_size;
	
	// Construct paginator
	// @param 	array 	$data
	// @param 	int 	$offset
	// @param 	int 	$limit
	public function __construct(array $data, $offset = 1, $limit=1)
	{
		$this->_data = $data;
		$this->_size = count($this->_data);
		$this->_limit = (int) $limit;
		$this->_offset = (int) $offset;
	}
	
	// Returns items for next page
	public function getNext()
	{
		$this->_offset += $this->_limit;
		$this->_resetMaxOffset();
		return array_slice($this->_data, $this->_offset-1, $this->_limit);
	}
	
	// Creates and returns previous page
	public function getPrevious()
	{
		$this->_offset -= $this->_limit;
		$this->_resetMinOffset();
		return array_slice($this->_data, $this->_offset-1, $this->_limit);
	}
	
	// Returns items in current page
	public function getCurrent()
	{
		$this->_resetMaxOffset();
		$this->_resetMinOffset();
		return array_slice($this->_data, $this->_offset-1, $this->_limit);
	}
	
	// Returns item for a specified page
	public function getPage($page)
	{
		$maxOffset = $this->_limit * $page;
		$this->_offset = $maxOffset - $this->_limit;
		$this->_offset+=1;
		$this->_resetMaxOffset();
		$this->_resetMinOffset();
		return array_slice($this->_data, $this->_offset-1, $this->_limit);
	}
	
	// Returns current offset
	public function getOffset()
	{
		return $this->_offset;
	}
	
	// Sets current offset
	public function setOffset($offset)
	{
		$this->_offset = $offset;
		$this->_resetMaxOffset();
		$this->_resetMinOffset();
	}
	
	// Returns the last item to display on a page
	public function getLastIndex()
	{
		$maxOffset = ($this->_offset-1) + $this->_limit;
		return $maxOffset;
	}
	
	// Computes and returns total number of pages
	public function getTotalPages()
	{
		return ceil($this->_size/$this->_limit);
	}
	
	//
	public function getCurrentPageIndex()
	{
		// Showing page 2 of totalPages
	}
	
	// Returns size of data
	public function getSize()
	{
		return $this->_size;
	}
	
	/**
	 * Checks that maximum offset is not exceeded
	 */
	protected function _resetMaxOffset()
	{
		if($this->_offset > $this->_size)
		{
			$this->_offset = $this->_size - $this->_limit;
			$this->_offset+=1;
			trigger_error("Maximum offset exceeded", E_USER_ERROR);
		}
	}
	
	/**
	 * Checks that minimum offset is not exceeded
	 */
	protected function _resetMinOffset()
	{
		if($this->_offset < 1)
		{
			$this->_offset = 1;
			trigger_error("Minimum offset reached", E_USER_ERROR);
		}
	}
}

/*
$paginator = new Paginator(range(1, 1000), 1, 10);

$result = $paginator->getCurrent();
echo "Current: Showing " . $paginator->getOffset() . ' - ' . $paginator->getLastIndex() . ' of ' . $paginator->getSize();
var_dump($result);


$result = $paginator->getNext();
echo "Next: Showing " . $paginator->getOffset() . ' - ' . $paginator->getLastIndex() . ' of ' . $paginator->getSize();
var_dump($result);

$result = $paginator->getNext();
echo "Next: Showing " . $paginator->getOffset() . ' - ' . $paginator->getLastIndex() . ' of ' . $paginator->getSize();
var_dump($result);

var_dump("Total Pages: " . $paginator->getTotalPages());

$result = $paginator->getPage(10);
echo "Next: Showing " . $paginator->getOffset() . ' - ' . $paginator->getLastIndex() . ' of ' . $paginator->getSize();
var_dump($result);

*/