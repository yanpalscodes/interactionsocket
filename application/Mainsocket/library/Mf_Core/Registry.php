<?php
/**
 *
 */
namespace Mf_Core;

use \Exception;

/**
 * Registry
 *
 * A centralized storage for shared config and application
 * objects.
 *
 * Components make use of the Registry to store runtime data
 * to be accessed anywhere by other components.
 *
 * @see http://en.wikipedia.org/wiki/Windows_Registry
 */
class Registry
{
	/**
	 * Registry container
	 *
	 * @var 	array
	 */
	protected $_objects;

	/**
	 * Single Registry instance
	 *
	 * @var 	Registry
	 */
	protected static $_instance;

	/**
	 *
	 */
	protected function __construct()
	{
		$this->_objects = array();
	}

	/**
	 *
	 */
	public static function getInstance()
	{
		if(!self::$_instance)
		{
			self::$_instance = new self;
		}

		return self::$_instance;
	}

	/**
	 * Prevents cloning
	 */
	public function __clone()
	{
		return $this->_objects;
	}

	/**
	 *
	 */
	public function set($key, $value)
	{
		$old = isset($this->_objects[$key]) ? $this->_objects[$key] : null;
		if($old == $value) return true;

		$this->_objects[$key] = $value;
		return true;
	}

	/**
	 * 
	 */
	public function get($key)
	{
		if(!isset($this->_objects[$key]))
		{
			throw new Exception("Object with key " . $key . " missing in Registry");
		}
		
		$value = $this->_objects[$key];
		return $value;
	}

	/**
	 *
	 */
	public function remove($key)
	{
		if(isset($this->_objects[$key]))
		{
			unset($this->_objects[$key]);
			return true;
		}

		return false;
	}
}
