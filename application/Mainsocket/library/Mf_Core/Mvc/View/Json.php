<?php
/**
 *
 */
namespace Mf_Core\Mvc\View;

use \Exception;
use \Mf_Core\Mvc\Controller;
use \Mf_Core\Mvc\View;

/**
* 
*/
abstract class Json extends View
{
	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_format = 'json';
	}

	/**
	 * Renders JSON encoded data
	 */
	public function display()
	{
		// Set header for json
		header('Content-Type: application/json');

		// This will prompt for download
		// header('Content-disposition: attachment; filename="' . $this->getName() . '.json"');

		// Display
		parent::display();
	}
}