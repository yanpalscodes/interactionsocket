<?php
/**
 *
 */
namespace Mf_Core\Auth;

use \Mf_Core\Auth\Handler;
use \Mf_Core\Registry;

/**
 * Authentication
 *
 * Multi-purpose Authentication System
 * Lets your write custom authentication callbacks to
 * handle any kind of authentication.
 *
 * Handles CSRF validation.
 *
 * Only singleton Auth instance is supported at the moment.
 */
class Auth
{
	/**
	 *
	 */
	protected $_handler;

	/**
	 * Create Auth object
	 *
	 * @param 	Mf_Core\Auth\HandlerInterface 	$authHandler
	 */
	public function __construct(Handler $authHandler)
	{
		$this->_handler = $handler;
	}

	/**
	 * Method to carry out authentication at authentication point.
	 *
	 * Pass credentials to handler to authenticate.
	 * AuthHandler may store data in session that will be used to check if object
	 * was authenticated
	 *
	 * @return 	bool 		True on sucess, false on failure
	 */
	public function authenticate(array $credentials = array())
	{
		if(!$this->_handler)
		{
			return true;
		}

		$params = array('credentials'=>$credentials);
		return call_user_func_array(array($this->_handler, 'authenticate'), $params);
	}

	/**
	 * Method to ensure that object e.g User has been
	 * authenticated
	 *
	 * The handler should return to authentication point e.g page or authentication form
	 * where object has to provide credentials if object wasn't already authenticated.
	 *
	 * @return 		Void
	 */
	public function mustAuthenticate()
	{
		if(!$this->_handler)
		{
			return true;
		}

		$this->_handler->validate();
	}

	/**
	 * Destroys previous authentication
	 */
	public function destroy()
	{
		if(!$this->_handler)
		{
			return true;
		}

		$this->_handler->destroy();
	}

	/**
	 * Returns AuthHandler
	 */
	public function getHandler()
	{
		return $this->_handler;
	}
	
	/**
	 * Get the token for authentication
	 */
	public function getToken()
	{
		$this->_handler->getToken();
	}

	/**
	 * Registers Auth in a Registry for easy access.
	 *
	 * Use can access Auth object via: 
	 * Registry::getInstance()->get('NameOfHandlerClass');
	 */
	public function register(Registry $registry)
	{
		$key = strtolower(get_class($this->_handler));
		$registry->set($key, $this);
	}
}


