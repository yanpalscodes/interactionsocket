<?php
/**
 *
 *
 */
namespace Mf_Core\Config;

use Exception;

/**
 * Php format configuration file parser.
 */
class Php extends Handler
{
	/**
	 * $var 	string
	 */
	protected $_type = 'Php';

	/**
	 *
	 */
	protected function __construct()
	{

	}

	/**
	 * Parses a configuration file
	 */
	public function parse($file)
	{
		if(!is_readable($file))
		{
			throw new Exception('Unable to read configuration file', 500);
		}

		require_once $file;

		if(!isset($config) || empty($config))
		{
			$config = array();
		}

		$parsed = array();
		foreach($config as $path => $value)
		{
			$tmp = $this->_cleanPath($path);

			// Excess chaining is not supported
			if(sizeof($tmp) > 2) continue;
			if(sizeof($tmp) === 1)
			{
				$parsed[$tmp[0]] = $value;
			}
			else
			{
				if(isset($parsed[$tmp[0]]) && !is_array($parsed[$tmp[0]]))
				{
					$parsed[$tmp[0]] = array();
				}

				$parsed[$tmp[0]] = isset($parsed[$tmp[0]]) ? $parsed[$tmp[0]] : array();
				$parsed[$tmp[0]][$tmp[1]] = $value;
			}
		}

		// Convert to object
		array_walk($parsed, function(&$value, $key) {
			if(is_array($value))
			{
				$value = (object) $value;
			}
		});

		$parsed = (object) $parsed;

		return $parsed;
	}

	/**
	 * - Removes trailing dot from $path
	 *
	 * @return 	array
	 */
	protected function _cleanPath($path)
	{
		return array_values(array_filter(explode('.', $path), 'strlen'));
	}

}