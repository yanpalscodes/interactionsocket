<?php
/**
 *
 */
namespace Mf_Core\Config;

use \Exception;
use Mf_Core\Config\Handler;

/**
 * Mf_Core\Application\Config
 *
 * Global configuration file.
 * Only one config file and format must be maintained
 * 	throughout your application.
 *
 * How config data is stored:
 * - ini_set
 * - ini_get
 */
class Config
{
	/**
	 * Object representation config data.
	 *
	 * @var 	object
	 */
	protected $_data;

	/**
	 * Writable/readonly flag
	 *
	 * @var 	bool
	 */
	protected $_writable;

	/**
	 * Config parser
	 *
	 *
	 */
	protected $_handler;

	/**
	 *
	 */
	public static $_instance = null;

	/**
	 * Creates a config
	 *
	 * - Loads config file
	 * - Parses config file once
	 *
	 * @throws Exception
	 */
	protected function __construct($file, Handler $handler, $writable=0)
	{
		if(!is_readable($file))
		{
			throw new Exception("Missing configuration file", 500);
		}

		$this->_handler = $handler;
		$this->_writable = $writable;

		// Load and parse config file
		$this->_data = $this->_parse($file);
	}

	/**
	 * Creates singleton or global application config
	 *
	 * @return Mf_Core\Config
	 */
	public static function getInstance($file = null, $type = 'Php', $writable=0)
	{
		if(!self::$_instance)
		{
			$handler = Handler::createhandler($type);
			self::$_instance = new self($file, $handler, $writable);
		}

		return self::$_instance;
	}

	/**
	 * Parses a config file and loads config content once
	 * into application memory.
	 *
	 * Parser must return an object
	 */
	protected function _parse($file)
	{
		$data = $this->_handler->parse($file);

		if(!is_object($data))
		{
			throw new Exception("Config parser error. Invalid config.");
		}

		return $data;
	}

	/**
	 *
	 */
	public function isWritable()
	{
		return !!$this->_writable;
	}

	/**
	 * Gets a option from config
	 *
	 * @return 	mixed 	Returns an object if node is a parent node
	 *					else the node's value is returned
	 */
	public function get($path)
	{
		$nodes = $this->_cleanPath($path);
		if(empty($nodes) || count($nodes) > 2) return false;

		$data = null;
		if(count($nodes) === 2)
		{
			if(isset($this->_data->{$nodes[0]}))
			{
				$data = isset($this->_data->{$nodes[0]}->{$nodes[1]})
					? $this->_data->{$nodes[0]}->{$nodes[1]} : $data;
			}
		}
		else
		{
			$data = isset($this->_data->{$nodes[0]}) ? $this->_data->{$nodes[0]} : $data;
		}

		return $data;
	}

	/**
	 * Sets an option in the application config
	 * This will not change the actual content of the configuration
	 * file.
	 *
	 * Sample params: 'host.', 'database.name', 'database'
	 *
	 * @return 	bool 	Returns true if value set, else false
	 */
	public function set($path, $value)
	{
		if(!$this->isWritable())
		{
			throw new Exception("Config is readonly", 500);
		}

		// Prevent setting rules that are not in the config file.
		// if(!$this->exists($path) || !$path) return false;

		$old = $this->get($path);
		if (is_object($old) || strcasecmp($old, $value) == 0) return false;

		$nodes = $this->_cleanPath($path);
		if(empty($nodes) || count($nodes) > 2) return false;

		// populate node
		if(count($nodes) === 2)
		{
			if(isset($this->_data->{$nodes[0]}))
			{
				// Write value
				$this->_data->{$nodes[0]}->{$nodes[1]} = $value;
				return true;
			}
			else {
				// Create node and write value
				$this->_data->{$nodes[0]} = (object) array();
				$this->_data->{$nodes[0]}->{$nodes[1]} = $value;
				return true;
			}
		}
		else {
			$this->_data->{$nodes[0]} = $value;
			return true;
		}

		return false;
	}

	/**
	 * More configuration options can be set
	 * by passing an array, after the config file is loaded.
	 */
	public function setOptions(array $options)
	{
		foreach ($options as $path => $value)
		{
			if(is_array($value))
			{
				// recursive
				$this->setOptions($value);
			}
			else
			{
				$this->set($path, $value);
			}
		}

		return true;
	}

	/**
	 * - Remove trailing dot from $path
	 *
	 * @return 	array
	 */
	protected function _cleanPath($path)
	{
		return array_values(array_filter(explode('.', $path), 'strlen'));
	}

	/**
	 * Says if a config path exists
	 *
	 * @return 	bool
	 */
	 public function exists($path)
	 {
	 	return !!$this->get($path);
	 }

	/**
	 * Merge another config object into this one, recursively.
	 *
	 * It reads config data stored in one config file
	 * and sets them on this one.
	 *
	 * - A duplicate key in teh $source will override this one.
	 */
	public function merge(Config $source)
	{
		throw new Exception("Merging not yet supported", 500);
		
	}
}
