<?php
/**
 *
 */
namespace Mf_Core\Session;

use \Mf_Core\Session\Handler as SessionHandler;

/**
 * Session
 *
 * Not using Joomla's SessionStorage Factory pattern makes it
 * easier to maintain and scale one SessionHandler throughout
 * your application. [Be consistent]
 *
 * CUSTOM SESSION HANDLING:
 * You can write a CustomHandler and register with
 * the Session object before session is started.
 * - Your custom session must handler either extend
 *   \Mf_Core\Session\Handler or implement php's \SessionHandlerInterface
 * - autostart option must not be true during session instantiation.
 *
 * USES OF SESSION:
 * - Track user behaviour
 * 	- Prevent a user from making same action twice for a certain time limit
 * - Track page views of a user
 * - Store user preferred choices
 * - Store user picked items while that're shopping
 */
class Session 
{
	// Id generated for current session
	protected $_id;
	
	// Session name
	protected $_name;
	
	// Cookie lifetime
	protected $_lifetime;
	
	// Cookie path
	protected $_path;
	
	// Cookie domain
	protected $_domain;
	
	// Cookie is sent over SSL
	protected $_secure;
	
	// Cookie is httponly
	protected $_httponly;

	// Custom session handler
	protected $_handler;
	
	// Singleton Session instance
	protected static $_instance;
	
	//
	public function __construct(array $options, SessionHandler $handler = null, $autostart = false)
	{
		// Set session name or use php configuration default
		$this->_name = isset($options['name']) ? $options['name'] : session_name();
		
		// Set session cookie params
		$this->_lifetime = isset($options['lifetime']) ? $options['lifetime'] : ini_get('session.cookie_lifetime');
		$this->_path = isset($options['path']) ? $options['lifetime'] : ini_get('session.cookie_path');
		$this->_domain = isset($options['domain']) ? $options['domain'] : ini_get('session.cookie_domain');
		$this->_secure = (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) != 'off')) ? true : false;
		$this->_httponly = isset($options['httponly']) ? $options['httponly'] : ini_get('session.cookie_httponly');
		
		// Set handler
		$this->_handler = $handler;
		
		// Start session if autostart is on
		if($autostart) { $this->start(); }
	}
	
	public static function getInstance(array $options = array(), SessionHandler $handler = null, $autostart = false)
	{
		if(!self::$_instance)
		{
			self::$_instance = new self($options, $handler, $autostart);
		}
		
		return self::$_instance;
	}
	
	// Sets session handler
	public function setHandler(SessionHandler $handler)
	{		
		$this->_handler = $handler;
	}
	
	//
	public function getHandler()
	{
		return $this->_handler;
	}
	
	// Registers session handler
	// Must be called once before session_start()
	protected function _registerHandler()
	{
		if($this->_handler === null)
		{
			return false;
		}

		// session_set_save_handler($this->_handler);
		session_set_save_handler(
			array($this->_handler, 'open'),
			array($this->_handler, 'close'),
			array($this->_handler, 'read'),
			array($this->_handler, 'write'),
			array($this->_handler, 'destroy'),
			array($this->_handler, 'gc')
		);
	}
	
	// Starts session.
	//
	// Explicitly destroys old session and
	// creates a new one
	public function start()
	{
		// Ensure that session_start() is called once
		if(session_id())
		{
			session_regenerate_id(true);
			$this->destroy();
		}
		
		// Register cookie params
		session_set_cookie_params($this->_lifetime, $this->_path, $this->_domain, $this->_secure, $this->_httponly);
		
		// Register set handler
		$this->_registerHandler();

		// Set session name
		session_name(md5($this->_name));
		
		// Starts session and stores session id
		// in session_id() function by either getting one from
		// previous request or creating a new one.
		session_start();
		
		// Store the session id
		$this->_id = session_id();
	}
	
	// 
	public function getName()
	{
		return $this->_name;
	}
	
	// Writes data to session.
	// Note: Objects and arrays are serialized.
	public function write($key, $value, $namespace = 'global')
	{
		if ($value === null)
		{
			// Remove session data
			$this->remove($key, $namespace);
		}
		else
		{
			$namespace = '__' . $namespace;
		
			// Explicitly serialize object or array
			if(is_object($value) || is_array($value))
			{
				$value = serialize($value);
			}
			
			$_SESSION[$namespace][$key] = $value;
		}

		return true;
	}
	
	// @param 	$serialized 	bool	Set to true if you expect object or array
	public function read($key, $serialized = false, $namespace = 'global')
	{
		// Writing your session to different namespaces
		// can be useful when you need to store data with
		// same name for different contexts.
		$namespace = '__' . $namespace;
		
		$value = null;

		if (isset($_SESSION[$namespace][$key]))
		{
			$value = $_SESSION[$namespace][$key];
		}
		
		// Need to decode value
		if($value && $serialized)
		{
			$value = unserialize($value);
		}
		
		return $value;
	}
	
	// Removes session data
	public function remove($key, $namespace = 'global')
	{
		$namespace = '__' . $namespace;
		
		if(isset($_SESSION[$namespace][$key]))
		{
			unset($_SESSION[$namespace][$key]);
		}
		
		return true;
	}
	
	// Destroys session
	public function destroy()
	{
		// Kill the session completely
		if(isset($_COOKIE[session_name()]))
		{
			$cookie = session_get_cookie_params();
			$name = $this->_name ? $this->_name : session_name();
			setcookie($name, '', time()-3600, $cookie['path'], $cookie['domain']);
		}

		session_destroy();
		session_unset();
	}
}

/*
$session = Session::getInstance(array(
	'name' => 'smartkon',
	'domain' => 'localhost',
	'httponly' => 1
), null, true);
var_dump($session);

// Start
$session->start();
var_dump($session);

// Write
$session->write('user', array('christer', 'annie'));
var_dump($_SESSION);

// Read
var_dump($session->read('user', true));

// Remove
$session->remove('users');
var_dump($_SESSION);

var_dump(session_id());

// $session->start();
*/