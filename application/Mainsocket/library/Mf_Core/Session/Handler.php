<?php
/**
 *
 */
namespace Mf_Core\Session;

use \SessionHandlerInterface;

/**
 * Abstract Session Handler
 *
 * This class implements the internal PHP SessionHandlerInterface
 */
abstract class Handler implements SessionHandlerInterface
{
	//
	protected static $_instance;
	
	//
	protected function __construct(array $options = array())
	{
		// Does noting with options
		// ini_set('session.save_handler', 'files');
		// ini_set('session.save_handler', 'user');
	}
	
	//
	public static function getInstance(array $options = array())
	{
		if(!isset(self::$_instance))
		{
			self::$_instance = new self($options);
		}
		
		return self::$_instance;
	}
	
	//
	public function close()
	{
		return true;
	}

	//
	public function destroy($session_id)
	{
		return true;
	}

	//
	public function gc($maxlifetime)
	{
		return true;
	}

	//
	public function open($save_path, $session_id )
	{
		return true;
	}

	//
	public function read($session_id)
	{
		return true;
	}

	//
	public function write($session_id, $session_data)
	{
		return true;
	}
}