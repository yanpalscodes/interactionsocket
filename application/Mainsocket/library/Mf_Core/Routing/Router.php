<?php
/**
 *
 */
namespace Mf_Core\Routing;

use \Exception;
use \ReflectionClass;
use \Mf_Core\Uri;
use \Mf_Core\Routing\Helper;

/**
 * Router / Dispatcher
 *
 * If strict mode is turned on, the router
 * will throw an Exception when it finds a method in the controller
 * that matches the the route but has less params than supplied.
 *
 * Not enabling a Router strict mode makes it greedy.
 *
 * @throws Exception
 */
class Router
{
	/**
	 *
	 */
	protected $_uri;

	/**
	 * Base path
	 */
	protected $_base;

	/**
	 * Home controller
	 */
	protected $_defaultController;

	/**
	 * Default action
	 */
	protected $_defaultAction;

	/**
	 * Strict mode
	 */
	protected $_strict = 0;

	/**
	 * Router instance
	 */
	protected static $_instance;
	
	/**
	 * Constructs Router
	 * Configures router and builds route
	 *
	 * @param 	Uri 	$uri 		The URI to get route
	 * @param 	Config 	$config 
	 */
	protected function __construct(Uri $uri, array $options = array())
	{
		$this->_uri = $uri;
		$this->_strict = isset($options['strict']) ? !!$options['strict'] : false;
		$this->_base = isset($options['base']) ? $options['base'] : '';
		$this->_defaultController = isset($options['defaultController']) ? $options['defaultController'] : 'index';
		$this->_defaultAction = isset($options['defaultAction']) ? $options['defaultAction'] : 'view';
	}

	/**
	 * Creates singleton instance of Router
	 */
	public static function getInstance($uri, array $options = array())
	{
		if(!self::$_instance)
		{
			self::$_instance = new self($uri, $options);
		}

		return self::$_instance;
	}

	/**
	 * Converts URI to an internal route
	 */
	protected function _parseUri(Uri $uri)
	{
		$path = substr_replace( urldecode($uri->getPath()), '', 0, strlen($this->_base));
		return $this->_cleanPath($path);
	}

	/**
	 * Filter suspicious characters from route
	 */
	protected function _cleanPath($path)
	{
		return $path;
	}

	/**
	 * Splits a route into an array
	 */
	protected function _buildSegments($route)
	{
		$segments = ( array_filter( explode('/', $route), array($this, '_filterSegments') ));

		// Reset array keys
		$i = 0;
		foreach ($segments as $value) {
			$tmp = $value;
			array_splice($segments, $i, 1);
			$segments[$i] = $tmp;
			$i++;
		}

		return $segments;
	}

	/**
	 * Strips empty paths from route.
	 */
	protected function _filterSegments($value)
	{
		if(!strlen($value))
		{
			return false;
		}

		return true;
	}

	/**
	 * Loads and executes controller for current URI
	 */
	public function dispatch()
	{
		$output = $this->load($this->_uri);
		return $output;
	}

	/**
	 * Loads required uri
	 *
	 * Router::load('http://sitename.com/path')
	 */
	public function load(Uri $uri)
	{
		$output = "";
		$path = $this->_parseUri($uri);
		$segments = $this->_buildSegments($path);

		$controller = isset($segments[0]) ? $segments[0] : $this->_defaultController;
		$action = isset($segments[1]) ? $segments[1] : $this->_defaultAction;
		$params = isset($segments[2]) ? array_slice($segments, 2) : array();

		$output = $this->_executeController($controller, $action, $params);
		return $output;
	}

	/**
	 * Executes controller
	 *
	 * Performs checking to ensure that the requested controller and action exists
	 * It also check against action params to ensure that params parsed through
	 * route does not exceed required params, then creates named params.
	 *
	 * If it can't find a match, it throws an Exception with 404 code
	 *
	 * @param 	string  $name 		Name of controller
	 * @param 	string  $action 	Controller action
	 * @param 	array 	$params 	Method parameters
	 * @return
	 */
	protected function _executeController($name, $action, array $params = array())
	{
		try
		{
			$controller = Helper::loadController($name, $action);
			$className = get_class($controller);

			$r = new ReflectionClass($className);
			$rExec = $r->getMethod('execute');
			$rParams = $rExec->getParameters();

			if($this->_strict && (count($params) > count($rParams)))
			{
				// Strict mode
				throw new Exception('EXCEPTION_CONTROLLER_ACTION_PARAMS_EXCEEDED', 404);
			}

			// Create named params
			$tmp = $params;
			$params = array();
			foreach($rParams as $index => $param)
			{
				// Get value from param, else get default value from method
				$val = !empty($tmp[$index]) ? $tmp[$index] : $param->getDefaultValue();
				$params[$param->getName()] = $val;
			}

			// Execute Controller
			ob_start();
			call_user_func_array(array($controller, 'execute'), $params);
			$contents = ob_get_contents();
			ob_end_clean();

			return $contents;
		}
		catch (Exception $ex)
		{
			throw new Exception(strtoupper($ex->getMessage()), 404);
		}
	}
}

/*@TODO
ROUTER EVENTS
============
$router->on('beforeRoute', function($route) {});
$router->trigger('beforeRoute');

ROUTER REWRITE
============
// Rewrites a matched route
$router->addRewrite(new RouteRewrite('url/dev/coin/', array(
    'controller' => ''
    'action'=>
    'params' => array()
));
*/