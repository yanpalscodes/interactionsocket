<?php
/**
 *
 */
namespace Mf_Core\Routing;
use \Mf_Core\Mvc\Controller;

/**
 *
 */
class Helper
{
	/**
	 * Loads controller
	 *
	 * @param 	string 	$name 		Controller name
	 * @param 	string 	$action 	Controller action
	 *
	 * @return Mf_Core\Mvc\Controller
	 */
	public static function loadController($name, $action)
	{
		return Controller::getInstance($name, $action);
	}
}