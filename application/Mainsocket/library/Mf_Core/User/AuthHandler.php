<?php
/**
 *
 */
namespace Mf_Core\User;

use \Mf_Core\Auth\Handler as AuthHandler;

/**
 * UserAuthHandler
 *
 * Handles user authentication: login and logout
 */
class AuthHandler extends AuthHandler
{
	/**
	 * Mf_Core\Session
	 */
	protected $_session;

	/**
	 *
	 */
	public function __construct(array $options)
	{
		parent::__construct($options);
		
		$this->_session = isset($options['session']) ? $options['session'] : $options['session'];
	}
	
	/**
	 * Method to authenticate
	 *
	 * @return 	bool 		Returns true on success
	 * @throws 	Exception 	If authentication failed
	 */
	public function authenticate(array $credentials = array())
	{
		$this->_login($credentials);
	}
	
	/**
	 * Verifys that object is authenticated
	 */
	public function validate()
	{
		if(!$this->_session->get('auth.user'))
		{
			return $this->redirectAuthUrl();
		}
		
		return $this->redirectSuccessUrl();
	}
	
	/**
	 *
	 */
	public function destroy()
	{
		$this->_logout();
	}

	/**
	 * Authenticates user if credentials are passed
	 * else redirects user to login url
	 */
	protected function _login(array $credentials = array())
	{
		// Perform login ...
		// Look up credentials in database
		// Set session flags
	}

	/**
	 * Destroys authentication data, and session
	 * Then redirects to login url.
	 */
	protected function _logout()
	{
		if($this->_session->getId())
		{
			$this->_session->set('auth.user', null);
			$this->_session->set('auth.token', null);

			$this->_session->destroy();
		}
		$this->_handler->destroy();
		$this->redirectAuthUrl();
	}
}