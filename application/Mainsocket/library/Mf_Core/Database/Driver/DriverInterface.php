<?php
/**
 *
 */
namespace Mf_Core\Database\Driver;

/**
 * 
 */
Interface DriverInterface
{
	public function __construct($host, $user, $passwd, $dbname);
}