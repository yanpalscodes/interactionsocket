<?php
/**
 *
 */
namespace Mf_Core\Database\Driver;

use \PDO as CorePDO;
use Mf_Core\Database\Driver\DriverInterface;

/**
 * 
 */
class Pdo extends CorePDO implements DriverInterface
{
	/**
	 * Host
	 */
	protected $_host;

	/**
	 * Username
	 */
	protected $_user;

	/**
	 * Password
	 */
	protected $_passwd;

	/**
	 * Name of database
	 */
	protected $_dbname;

	/**
	 *
	 */
	protected $_charset = 'UTF8';

	/**
	 * Database server in use Eg. mysql, postgresql, etc
	 */
	protected $_server = 'mysql';

	/**
	 *
	 */
	public function __construct($host, $user, $passwd, $dbname = null)
	{
		$this->_host = $host;
		$this->_user = $user;
		$this->_passwd = $passwd;
		$this->_dbname = $dbname;

		// Connect
		$dsn = 'mysql:dbname=' . $this->_dbname . ';host=' . $this->_host;
		$dsn .= ';charset=' . $this->_charset;
		$options = array();
		parent::__construct($dsn, $this->_user, $this->_passwd, $options);
	}

	/**
     *
     */
	public function setDatabase($dbname)
	{
		$this->_dbname = $dbname;
	}

	/**
	 *
	 */
	public function getDatabase()
	{
		return $this->_dbname;
	}

	/**
	 *
	 */
	public function setUser($user)
	{
		$this->_user = $user;
	}

	/**
	 *
	 */
	protected function getUser()
	{
		return $this->_user;
	}

	/**
	 *
	 */
	public function __destroy()
	{
		// $this->close();
	}

}

/*
//
// Joomla's Poor Implementation
//
// Builds a new database driver and wrapper methods
// for almost all methods available in the php API
//
class JDatabaseDriverPdo extends JDatabaseDriver
{
	protected $this->_connection;

	public function __construct()
	{
		// Create instance of PDO inside driver
		$this->_connection = new PDO($dsn, $user, $passwd, $options);
	}

	public function query($sql)
	{
		// Use connection query inside wrapper
		$this->_connection->query($sql);
	}
}

@see 	Other Joomla's Poor Code
		- Model
		- View
		- Controller
*/