<?php
/**
 * Mule			Alpha14 Framework
 *
 * @author		Christer G. <http://twitter.com/christergenius>
 * @version		Alpha 1.0 (also 1.0a).
 * @copyright 	(c) <year> Mule Coporation. All rights reserved.
 * @license		GNU General Public License version 2 or later; See LICENSE.txt
 */
namespace Mf_Core;

/**
 *
 */
class Debugger
{
	//
	// DEBUG_MODE
	//
	public static $_DEBUG_MODE = 0;
	
	//
	// Debug stack
	//
	protected static $_stack = array();
		
	/**
	 * Dumps data to _stack
	 *
	 * @param 	mixed	$data	Dump data to stack
	 */
	public static function dump($data, $exit = false)
	{
		if(!static::$_DEBUG_MODE)
		{
			return false;
		}

		// Collect debug trace
		$trace = debug_backtrace()[0];
		
		$dump = PHP_EOL . PHP_EOL . '<!-- (' . $trace['file'] . ' #' . $trace['line'] . ')' . PHP_EOL;
		$dump .= print_r( $data, true );
		$dump .= PHP_EOL . '-->';
		
		array_push(self::$_stack, $dump);

		if($exit)
		{
			static::render();
			exit;
		}
	}
	
	/**
	 * Renders dump stack
	 *
	 * @param 	void
	 */
	public static function render()
	{
		if(!static::$_DEBUG_MODE)
		{
			return false;
		}

		echo PHP_EOL . '<!-- BEGIN DEBUG -->';
		foreach(self::$_stack as $dump)
		{
			$output = print_r( $dump, true );

			/*
			if(extension_loaded('xdebug')
			{
				// var_dump will add formating html tags
				// if xdebug is enabled.
				
				// I WANT MY OWN STYLING
			}
			*/
			
			if( PHP_SAPI != 'cli')
			{
				$output = /*'<pre>' . */$output/* . '</pre>'*/;
			}
			
			echo $output;
		}
		echo PHP_EOL . '<!-- END DEBUG -->' . PHP_EOL;
		
		// Empty stack
		self::$_stack = array();
	}

	/**
	 *
	 *//*
	public function __destroy()
	{
		unset(self::$_stack);
	}
	*/
}