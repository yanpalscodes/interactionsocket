<?php
/**
 *
 */
namespace Mf_Core;

/**
 * Browser
 *
 * Provides information about a browser used by
 * a user via info gotted from $_SERVER['HTTP_USER_AGENT']
 * If you have enabled browsercap.ini in your php installation,
 * you can use get_browser() function
 *
 * Warning: Do not rely on Browser information.
 */
class Browser
{
	public static function getUa()
	{
		return $_SERVER['HTTP_USER_AGENT'];
	}
}
