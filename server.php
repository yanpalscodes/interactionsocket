#!/bin/php -q
<?php

error_reporting(E_ALL);

// Prevent script from timing out
set_time_limit(0);

/* Turn on implicit output flushing so we see what we're getting
 * as it comes in. */
ob_implicit_flush();

// Init
require_once("loader.php");

use Mf\WebSocket\Server;


//REMEMBER: Turn off firewall when using local IP apart from 127.0.0.1
//$server = Server::getInstance('127.0.0.1', '8000', array());
$server = Server::getInstance('159.65.22.6', '8000', array());



// Register chat
use Mf\MainSocket\CentralController;
$server->attachObserver(new CentralController());



//
// $server->attachObserver(new LiveBoard());

// Run
$server->run();
//-f "C:\wamp\www\wsengine\server.php"
