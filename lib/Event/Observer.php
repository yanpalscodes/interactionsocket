<?php
/**
 *
 */
namespace Mf_Core\Event;
use Mf_Core\Event\ObserverInterface;
use Mf_Core\Event\Event;

/**
 * Observes an event target for events that
 * may occur.
 *
 * Multiple events can be watched for by providing
 * methods for the different supported events.
 */
abstract class Observer implements ObserverInterface
{
	/**
	 *
	 */
	public function __construct()
	{
	}
	
	/**
	 *
	 */
	public function update(Event $event)
	{
		$type = $event->getType();
		$method = 'on'.ucfirst($type);
		if(!method_exists($this, $method))
		{
			return false;
		}
		
		$result = call_user_func(array($this, $method), $event);
		return $result;
	}
}