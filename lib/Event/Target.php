<?php
/**
 *
 */
namespace Mf_Core\Event;
use Mf_Core\Event\TargetInterface;
use Mf_Core\Event\Event;
use Mf_Core\Event\Observer;

/**
 * Example Socket is an event listener
 */
abstract class Target implements TargetInterface
{
	/**
	 *
	 */
	protected $_observers = array();
	
	/**
	 *
	 */
	public function __construct()
	{
		
	}
	
	/**
	 * Attach or registers an observer (LIFO)
	 */
	public function attachObserver(Observer $observer)
	{
		array_unshift($this->_observers, $observer);
	}
	
	/**
	 * Detach an observer
	 */
	public function detachObserver(Observer $observer)
	{
		// Find index
		$index = array_search($observer, $this->_observers, true);
		if(!$index) {
			return false;
		}
		
		// Remove the listener
		array_splice($this->_observers, $index, 1);
		return true;
	}
	
	/**
	 * Notifies observers
	 */
	public function notify($eventType, array $params=array() )
	{
		foreach($this->_observers as $observer)
		{
			// Create event
			$event = new Event($eventType, $params);
			$event->setTarget($this);
			
			// Update listener of event
			$observer->update($event);
		}
	}
}
