<?php
/**
 *
 */
namespace Mf_Core\Event;
use Mf_Core\Event\Observer;

/**
 *
 */
Interface TargetInterface
{
	/**
	 * Attach or registers an observer
	 */
	public function attachObserver(Observer $observer);
	
	/**
	 * Detach an observer
	 */
	public function detachObserver(Observer $observer);
	
	/**
	 * Notifies observers
	 */
	public function notify($eventName);
}