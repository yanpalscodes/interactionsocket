<?php
/*
	Based on PHP WebSocket Server 0.2
	 - http://code.google.com/p/php-websocket-server/
	 - http://code.google.com/p/php-websocket-server/wiki/Scripting

	WebSocket Protocol 07
	 - http://tools.ietf.org/html/draft-ietf-hybi-thewebsocketprotocol-07
	 - Supported by Firefox 6 (30/08/2011)

	Whilst a big effort is made to follow the protocol documentation, the current script version may unknowingly differ.
	Please report any bugs you may find, all feedback and questions are welcome!
*/
/**
 * Mule Framework (WebSocket)
 */
namespace Mf\WebSocket;
use Mf\WebSocket\Client;
use Mf\WebSocket\Exception;
use Mf\WebSocket\Handshake\Request;
use Mf\WebSocket\Handshake\Response;
use Mf\WebSocket\Event\Target as WebSocketEventTarget;

// Chat
// use Mf\Chat\Chat;
 
/**
 * WebSocket/Server.php
 *
 * PHP Requirements:
 * - Enable sockets.dll extension in your php installation
 *
 * Features:
 * - Logging (Logs server errors, connections and disconnections)
 * - Event Model
 * - IP Filtering
 * - Origin Filtering
 * - Routing*
 */
class Server extends WebSocketEventTarget
{
	//
	// CONSTANTS
	//

	// amount of seconds a client has to send data to the server, before a ping request is sent to the client,
	// if the client has not completed the opening handshake, the ping request is skipped and the client connection is closed
	const WS_TIMEOUT_RECV = 10;

	// amount of seconds a client has to reply to a ping request, before the client connection is closed
	const WS_TIMEOUT_PONG = 5;

	// the maximum length, in bytes, of a frame's payload data (a message consists of 1 or more frames), this is also internally limited to 2,147,479,538
	const WS_MAX_FRAME_PAYLOAD_RECV = 100000;

	// the maximum length, in bytes, of a message's payload data, this is also internally limited to 2,147,483,647
	const WS_MAX_MESSAGE_PAYLOAD_RECV = 500000;

	//
	// INTERNAL
	//
	const WS_FIN =  128;
	const WS_MASK = 128;

	// Opcodes
	// Opcodes are instruction sets used by sockets to communicate internally
	// @see		https://developer.mozilla.org/en-US/docs/WebSockets/Writing_WebSocket_servers#Message_Fragmentation
	const WS_OPCODE_CONTINUATION = 0; // 0x00
	const WS_OPCODE_TEXT =         1; // 0x01
	const WS_OPCODE_BINARY =       2; // 0x02
	const WS_OPCODE_CLOSE =        8; // 0x08
	const WS_OPCODE_PING =         9; // 0x09
	const WS_OPCODE_PONG =         10; // 0x0A

	// Payload length
	const WS_PAYLOAD_LENGTH_16 = 126;
	const WS_PAYLOAD_LENGTH_63 = 127;

	// Client Ready States 
	const WS_READY_STATE_CONNECTING = 0;
	const WS_READY_STATE_OPEN =       1;
	const WS_READY_STATE_CLOSING =    2;
	const WS_READY_STATE_CLOSED =     3;

	// 
	const WS_STATUS_NORMAL_CLOSE =             1000;
	const WS_STATUS_GONE_AWAY =                1001;
	const WS_STATUS_PROTOCOL_ERROR =           1002;
	const WS_STATUS_UNSUPPORTED_MESSAGE_TYPE = 1003;
	const WS_STATUS_MESSAGE_TOO_BIG =          1004;

	const WS_STATUS_TIMEOUT = 3000;
	
	//
	// GLOBAL VARS
	//

	// @var 	resource 	Master socket
	private $_socket;
	
	// @var 	string 	Host address
	private $_host;
	
	// @var  	string 	Socket port
	private $_port;
	
	// @var 	array 	Connected client sockets
	private $_clients = array();
	
	// @var 	int 	Max client connections
	private $_maxClients = 100;
	
	// @var  	int 	Max connection per IP v4 at a time
	// 					Prevents DDOS Attack.
	private $_maxClientsPerIp = 15;
	
	// @var 	string 	Magic string. See wikipedia
	private $_acceptKey = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
	
	// @var 	array 	Allowed Ips including host
	private $_allowedIps = array();
	
	// @var 	
	private static $_instance = null;


	/**
	 *
	 */
	public function __construct($host, $port, array $config = array())
	{
		// create new master process
		if (!$this->_socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP))
		{
			throw new Exception("socket_create() failed: " . 
				socket_strerror(socket_last_error()), 500);
		}

		// Enable address reusable
		if (!socket_set_option($this->_socket, SOL_SOCKET, SO_REUSEADDR, 1))
		{
			throw new Exception("socket_set_option() failed: " . 
				socket_strerror(socket_last_error($this->_socket)), 500);
		}
		
		if (!socket_bind($this->_socket, $host, $port))
		{
			throw new Exception("socket_bind() failed: " .
				socket_strerror(socket_last_error($this->_socket)), 500);
		}
		
		if (!socket_listen($this->_socket, 10))
		{
			throw new Exception("socket_listen() failed: " .
				socket_strerror(socket_last_error($this->_socket)), 500);
		}

		// Cache arguments
		$this->_host = $host;
		$this->_port = $port;
		
		// Configure server
		$this->_config($config);
		
		// Add host to allowed ips
		$this->_allowedIps = array($host);
	}
	
	/**
	 *
	 */
	public static function getInstance($host = null, $port = null, $config = array())
	{
		if(is_null(self::$_instance))
		{
			$host = gethostbyname($host);
			self::$_instance = new self($host, $port, $config);
		}
		
		return self::$_instance;
	}
	
	/**
	 * Runs the server
	 */
	public function run()
	{
		$nextPingCheck = time() + 1;
		while (isset($this->_socket))
		{
			var_dump(count($this->_clients));
			
			$writeSockets = $exceptSockets = NULL;
			$readSockets = array($this->_socket);
			foreach($this->_clients as $client)
			{
				$readSockets[count($readSockets)] = $client->socket;
			}
			
			$result = socket_select($readSockets, $writeSockets, $exceptSockets, 1);
			if ($result === false)
			{
				// @ERROR: CRITICAL
				throw new Exception("socket_select() failed: " .
					socket_strerror(socket_last_error()), 500);
			}
			elseif ($result > 0)
			{
				foreach ($readSockets as $socket)
				{
					if($socket === $this->_socket)
					{
						// Master socket changed
						$client = socket_accept($this->_socket);
						if ($client !== false)
						{
							try 
							{
								// @TODO validate request header
								
								// Add client if maxClients is not exceeded
								if ($this->countClients() < $this->_maxClients)
								{
									$this->addClient($client);
								}
								else {
									socket_close($client);
								}
							}
							catch (Exception $ex)
							{	// Not a valid client
								socket_close($client);
							}
						}
					}
					else
					{
						$buffer = '';
						$bytes = @socket_recv($socket, $buffer, 4096, 0);
						if ($bytes === false)
						{
							// error on recv, remove client socket (will check to send close frame)
							$this->sendClientClose($socket, self::WS_STATUS_PROTOCOL_ERROR);
						}
						elseif ($bytes > 0)
						{
							// var_dump($buffer);
							
							// process handshake or frame(s)
							if (!$this->processClient($socket, $buffer, $bytes))
							{
								$this->sendClientClose($socket, self::WS_STATUS_PROTOCOL_ERROR);
							}
						}
						else {
							// 0 bytes received from client, meaning the client closed the TCP connection
							$this->removeClient($socket);
						}
					}
				}
			}

			if (time() >= $nextPingCheck) {
				$this->checkIdleClients();
				$nextPingCheck = time() + 1;
			}
		}
	}
	
	/**
	 * Closes a client (socket)
	 */
	function close($socket)
	{
		$client = $this->findClientBySocket($socket);
		return $this->sendClientClose($client->socket, self::WS_STATUS_NORMAL_CLOSE);
	}
	
	/**====================================
	 * CONFIGURATION
	 *========================================*/
	
	/**
	 * Configures server once
	 */
	private function _config(array $config)
	{
		$configurable = array(
			'maxClients',
			'maxUniqueClients'
		);
		
		foreach($config as $option => $value)
		{
			if(in_array($option, $configurable))
			{
				// Set option
				$this->{'_'.$option} = $value;
			}
		}
	}
	
	/**=========================================
	 * CLIENTS
	 *==========================================*/
	 
	/**
	 *
	 */
	public function countClients()
	{
		return count($this->_clients);
	}
	
	/**
	 * Returns the number to unique clients by IP.
	 * ( Similar to counting external IPs connected to server. )
	 */
	public function countGroupClients()
	{
		$clients = array();
		foreach($this->_clients as $client)
		{
			if(!isset($clients[$client->ip]))
			{
				$clients[$client->ip] = array();
			}
			
			$clients[$client->ip][] = $client;
		}
		
		return count($clients);
	}
	
	/**
	 * Returns the number of clients that are connected on the same IP
	 *
	 * @param 	string	$ip		
	 */
	public function countClientsFromIp($ip)
	{
		$numClients = 0;
		foreach($this->_clients as $client)
		{
			if($client->ip == $ip)
				$numClients++;
		}
		
		return $numClients;
	}
	
	/**
	 *
	 */
	public function getClients()
	{
		return $this->_clients;
	}

	/**
	 * Adds a client
	 */
	public function addClient($socket)
	{
		// Create client
		$client = new Client($socket);
		
		// store initial client data
		$client->messageBuffer = '';
		$client->readyState = self::WS_READY_STATE_CONNECTING;
		$client->lastRecvTime = time();
		$client->pingSentTime = false;
		$client->closeStatus = 0;
		$client->framePayloadDataLength = false;
		$client->frameBytesRead = 0;
		$client->frameBuffer = '';
		$client->messageOpcode = 0;
		$client->messageBufferLength = 0;
		
		// Push client
		$this->_clients[] = $client;
	}

	/**
	 * Removes a client
	 */
	function removeClient($socket)
	{
		$client = $this->findClientBySocket($socket);
		
		// fetch close status (which could be false), and call ONCLOSE event callback
		$closeStatus = $client->closeStatus;

		// close socket
		socket_close($client->socket);

		// @TODO ONDISCONNECT
		$this->notify('disconnect', array(
			'client' => $client
		));

		// remove socket and client data from arrays
		$index = array_search($client, $this->_clients);
		array_splice($this->_clients, $index, 1);
	}

	/**
	 * Searches for client by socket resource
	 */
	public function findClientBySocket($socket)
	{
		foreach($this->_clients as $client)
		{
			if($client->socket === $socket)
			{
				return $client;
			}
		}
		
		return null;
	}
	
	/**
	 * Checks for idle clients at intervals,
	 * disconnects and removes client.
	 *
	 * Idle clients are detected by checking the last time
	 * the client sent data
	 */
	public function checkIdleClients()
	{
		$time = time();
		foreach ($this->_clients as $index => $client)
		{
			if ($client->readyState != self::WS_READY_STATE_CLOSED)
			{
				// client ready state is not closed
				if ($client->pingSentTime !== false)
				{
					// ping request has already been sent to client, pending a pong reply
					if ($time >= $client->pingSentTime + self::WS_TIMEOUT_PONG)
					{
						// client didn't respond to the server's ping request in self::WS_TIMEOUT_PONG seconds
						$this->sendClientClose($client->socket, self::WS_STATUS_TIMEOUT);
						$this->removeClient($client->socket);
					}
				}
				elseif ($time >= $client->lastRecvTime + self::WS_TIMEOUT_RECV)
				{
					// last data was received >= self::WS_TIMEOUT_RECV seconds ago
					if ($client->readyState != self::WS_READY_STATE_CONNECTING)
					{
						// client ready state is open or closing
						// PING: Send a ping to client
						$this->findClientBySocket($client->socket)->pingSentTime = time();
						$this->sendClientMessage($client->socket, self::WS_OPCODE_PING, '');
					}
					else {
						// client ready state is connecting
						$this->removeClient($client->socket);
					}
				}
			}
		}
	}

	/**
	 *
	 */
	function sendClientClose($socket, $status=false)
	{
		$client = $this->findClientBySocket($socket);
		
		// check if client ready state is already closing or closed
		if ($client->readyState == self::WS_READY_STATE_CLOSING
		|| $client->readyState == self::WS_READY_STATE_CLOSED)
			return true;

		// store close status
		$client->closeStatus = $status;

		// send close frame to client
		$status = $status !== false ? pack('n', $status) : '';
		$this->sendClientMessage($client->socket, self::WS_OPCODE_CLOSE, $status);

		// set client ready state to closing
		$client->readyState = self::WS_READY_STATE_CLOSING;
	}
	
	/**
	 * Sends data to client
	 */
	function sendClientMessage($socket, $opcode, $message)
	{
		$client = $this->findClientBySocket($socket);
		
		// check if client ready state is already closing or closed
		if ($client->readyState == self::WS_READY_STATE_CLOSING
		|| $client->readyState == self::WS_READY_STATE_CLOSED)
			return true;

		// fetch message length
		$messageLength = strlen($message);

		// set max payload length per frame
		$bufferSize = 4096;

		// work out amount of frames to send, based on $bufferSize
		$frameCount = ceil($messageLength / $bufferSize);
		if ($frameCount == 0) $frameCount = 1;

		// set last frame variables
		$maxFrame = $frameCount - 1;
		$lastFrameBufferLength = ($messageLength % $bufferSize) != 0 ? ($messageLength % $bufferSize) : ($messageLength != 0 ? $bufferSize : 0);

		// loop around all frames to send
		for ($i=0; $i<$frameCount; $i++) {
			// fetch fin, opcode and buffer length for frame
			$fin = $i != $maxFrame ? 0 : self::WS_FIN;
			$opcode = $i != 0 ? self::WS_OPCODE_CONTINUATION : $opcode;

			$bufferLength = $i != $maxFrame ? $bufferSize : $lastFrameBufferLength;

			// set payload length variables for frame
			if ($bufferLength <= 125) {
				$payloadLength = $bufferLength;
				$payloadLengthExtended = '';
				$payloadLengthExtendedLength = 0;
			}
			elseif ($bufferLength <= 65535) {
				$payloadLength = self::WS_PAYLOAD_LENGTH_16;
				$payloadLengthExtended = pack('n', $bufferLength);
				$payloadLengthExtendedLength = 2;
			}
			else {
				$payloadLength = self::WS_PAYLOAD_LENGTH_63;
				$payloadLengthExtended = pack('xxxxN', $bufferLength); // pack 32 bit int, should really be 64 bit int
				$payloadLengthExtendedLength = 8;
			}

			// set frame bytes
			$buffer = pack('n', (($fin | $opcode) << 8) | $payloadLength) . $payloadLengthExtended . substr($message, $i*$bufferSize, $bufferLength);

			// send frame
			$left = 2 + $payloadLengthExtendedLength + $bufferLength;
			do {
				$sent = @socket_send($client->socket, $buffer, $left, 0);
				if ($sent === false) return false;

				$left -= $sent;
				if ($sent > 0) $buffer = substr($buffer, $sent);
			}
			while ($left > 0);
		}

		return true;
	}

	/**
	 * Read client message and determine if it's
	 * handshake or normal message
	 *
	 * @return 	bool 	Returns FALSE if it can't understand message received
	 */
	function processClient($socket, &$buffer, $bufferLength)
	{
		$client = $this->findClientBySocket($socket);
		
		// Client already connected. Read message from client
		if ($client->readyState == self::WS_READY_STATE_OPEN)
		{
			$result = $this->buildClientFrame($client->socket, $buffer, $bufferLength);
		}
		// Client trying to connect. Handshake not completed
		elseif ($client->readyState == self::WS_READY_STATE_CONNECTING)
		{
			$result = $this->processClientHandshake($client->socket, $buffer);
			if ($result)
			{
				$client->readyState = self::WS_READY_STATE_OPEN;

				// @Event ONCONNECT
				$this->notify('connect', array(
					'client' => $client
				));
			}
		}
		else {
			$result = false;
		}

		return $result;
	}
	
	/**=======================================
	 * MESSAGING / FRAMING
	 *========================================*/
	
	/**
	 *
	 */
	function buildClientFrame($socket, &$buffer, $bufferLength)
	{
		$client = $this->findClientBySocket($socket);
		
		// increase number of bytes read for the frame, and join buffer onto end of the frame buffer
		$client->frameBytesRead += $bufferLength;
		$client->frameBuffer .= $buffer;

		// check if the length of the frame's payload data has been fetched, if not then attempt to fetch it from the frame buffer
		if ($client->framePayloadDataLength !== false || $this->checkSizeClientFrame($client->socket) == true)
		{
			// work out the header length of the frame
			$headerLength = ($client->framePayloadDataLength <= 125 ? 0 : ($client->framePayloadDataLength <= 65535 ? 2 : 8)) + 6;

			// check if all bytes have been received for the frame
			$frameLength = $client->framePayloadDataLength + $headerLength;
			if ($client->frameBytesRead >= $frameLength)
			{
				// check if too many bytes have been read for the frame (they are part of the next frame)
				$nextFrameBytesLength = $client->frameBytesRead - $frameLength;
				if ($nextFrameBytesLength > 0)
				{
					$client->frameBytesRead -= $nextFrameBytesLength;
					$nextFrameBytes = substr($client->frameBuffer, $frameLength);
					$client->frameBuffer = substr($client->frameBuffer, 0, $frameLength);
				}

				// process the frame
				$result = $this->processClientFrame($client->socket);

				// check if the client wasn't removed, then reset frame data
				if ($this->findClientBySocket($socket))
				{
					$client->framePayloadDataLength = false;
					$client->frameBytesRead = 0;
					$client->frameBuffer = '';
				}

				// if there's no extra bytes for the next frame, or processing the frame failed, return the result of processing the frame
				if ($nextFrameBytesLength <= 0 || !$result) return $result;

				// build the next frame with the extra bytes
				return $this->buildClientFrame($socket, $nextFrameBytes, $nextFrameBytesLength);
			}
		}

		return true;
	}
	
	/**
	 *
	 */
	function processClientFrame($socket)
	{
		$client = $this->findClientBySocket($socket);
		
		// store the time that data was last received from the client
		$client->lastRecvTime = time();

		// fetch frame buffer
		$buffer = &$client->frameBuffer;

		// check at least 6 bytes are set (first 2 bytes and 4 bytes for the mask key)
		if (substr($buffer, 5, 1) === false) return false;

		// fetch first 2 bytes of header
		$octet0 = ord(substr($buffer, 0, 1));
		$octet1 = ord(substr($buffer, 1, 1));

		$fin = $octet0 & self::WS_FIN;
		$opcode = $octet0 & 15;

		$mask = $octet1 & self::WS_MASK;
		if (!$mask) return false; // close socket, as no mask bit was sent from the client

		// fetch byte position where the mask key starts
		$seek = $client->framePayloadDataLength <= 125 ? 2 : ($client->framePayloadDataLength <= 65535 ? 4 : 10);

		// read mask key
		$maskKey = substr($buffer, $seek, 4);

		$array = unpack('Na', $maskKey);
		$maskKey = $array['a'];
		$maskKey = array(
			$maskKey >> 24,
			($maskKey >> 16) & 255,
			($maskKey >> 8) & 255,
			$maskKey & 255
		);
		$seek += 4;

		// decode payload data
		if (substr($buffer, $seek, 1) !== false) {
			$data = str_split(substr($buffer, $seek));
			foreach ($data as $key => $byte) {
				$data[$key] = chr(ord($byte) ^ ($maskKey[$key % 4]));
			}
			$data = implode('', $data);
		}
		else {
			$data = '';
		}

		// check if this is not a continuation frame and if there is already data in the message buffer
		if ($opcode != self::WS_OPCODE_CONTINUATION && $client->messageBufferLength > 0)
		{
			// clear the message buffer
			$client->messageBufferLength = 0;
			$client->messageBuffer = '';
		}

		// check if the frame is marked as the final frame in the message
		if ($fin == self::WS_FIN) {
			// check if this is the first frame in the message
			if ($opcode != self::WS_OPCODE_CONTINUATION)
			{
				// process the message
				return $this->processClientMessage($client->socket, $opcode, $data, $client->framePayloadDataLength);
			}
			else {
				// increase message payload data length
				$client->messageBufferLength += $client->framePayloadDataLength;

				// push frame payload data onto message buffer
				$client->messageBuffer .= $data;

				// process the message
				$result = $this->processClientMessage($client->socket, $client->messageOpcode, $client->messageBuffer, $client->messageBufferLength);

				// check if the client wasn't removed, then reset message buffer and message opcode
				if ($this->findClientBySocket($socket))
				{
					$client->messageBuffer = '';
					$client->messageOpcode = 0;
					$client->messageBufferLength = 0;
				}

				return $result;
			}
		}
		else {
			// check if the frame is a control frame, control frames cannot be fragmented
			if ($opcode & 8) return false;

			// increase message payload data length
			$client->messageBufferLength += $client->framePayloadDataLength;

			// push frame payload data onto message buffer
			$client->messageBuffer .= $data;

			// if this is the first frame in the message, store the opcode
			if ($opcode != self::WS_OPCODE_CONTINUATION)
			{
				$client->messageOpcode = $opcode;
			}
		}

		return true;
	}

	/**
	 *
	 */
	function processClientMessage($socket, $opcode, &$data, $dataLength)
	{
		$client = $this->findClientBySocket($socket);
		
		// check opcodes
		if ($opcode == self::WS_OPCODE_PING)
		{
			// received ping message
			return $this->sendClientMessage($client->socket, self::WS_OPCODE_PONG, $data);
		}
		elseif ($opcode == self::WS_OPCODE_PONG)
		{
			// received pong message (it's valid if the server did not send a ping request for this pong message)
			if ($client->pingSentTime !== false) {
				$client->pingSentTime = false;
			}
		}
		elseif ($opcode == self::WS_OPCODE_CLOSE)
		{
			// received close message
			if (substr($data, 1, 1) !== false)
			{
				$array = unpack('na', substr($data, 0, 2));
				$status = $array['a'];
			}
			else {
				$status = false;
			}

			if ($client->readyState == self::WS_READY_STATE_CLOSING)
			{
				// the server already sent a close frame to the client, this is the client's close frame reply
				// (no need to send another close frame to the client)
				$client->readyState = self::WS_READY_STATE_CLOSED;
			}
			else {
				// the server has not already sent a close frame to the client, send one now
				$this->sendClientClose($client->socket, self::WS_STATUS_NORMAL_CLOSE);
			}

			$this->removeClient($client->socket);
		}
		elseif ($opcode == self::WS_OPCODE_TEXT || $opcode == self::WS_OPCODE_BINARY)
		{
			// @TODO ONMESSAGE
			$this->notify('message', array(
				'client' => $client,
				'message' => $data
			));
		}
		else {
			// unknown opcode
			return false;
		}

		return true;
	}
	
	/**
	 *
	 */
	function checkSizeClientFrame($socket)
	{
		$client = $this->findClientBySocket($socket);
		
		// check if at least 2 bytes have been stored in the frame buffer
		if ($client->frameBytesRead > 1)
		{
			// fetch payload length in byte 2, max will be 127
			$payloadLength = ord(substr($client->frameBuffer, 1, 1)) & 127;

			if ($payloadLength <= 125)
			{
				// actual payload length is <= 125
				$client->framePayloadDataLength = $payloadLength;
			}
			elseif ($payloadLength == 126)
			{
				// actual payload length is <= 65,535
				if (substr($client->frameBuffer, 3, 1) !== false)
				{
					// at least another 2 bytes are set
					$payloadLengthExtended = substr($client->frameBuffer, 2, 2);
					$array = unpack('na', $payloadLengthExtended);
					$client->framePayloadDataLength = $array['a'];
				}
			}
			else {
				// actual payload length is > 65,535
				if (substr($client->frameBuffer, 9, 1) !== false)
				{
					// at least another 8 bytes are set
					$payloadLengthExtended = substr($client->frameBuffer, 2, 8);

					// check if the frame's payload data length exceeds 2,147,483,647 (31 bits)
					// the maximum integer in PHP is "usually" this number. More info: http://php.net/manual/en/language.types.integer.php
					$payloadLengthExtended32_1 = substr($payloadLengthExtended, 0, 4);
					$array = unpack('Na', $payloadLengthExtended32_1);
					if ($array['a'] != 0 || ord(substr($payloadLengthExtended, 4, 1)) & 128)
					{
						$this->sendClientClose($client->socket, self::WS_STATUS_MESSAGE_TOO_BIG);
						return false;
					}

					// fetch length as 32 bit unsigned integer, not as 64 bit
					$payloadLengthExtended32_2 = substr($payloadLengthExtended, 4, 4);
					$array = unpack('Na', $payloadLengthExtended32_2);

					// check if the payload data length exceeds 2,147,479,538 (2,147,483,647 - 14 - 4095)
					// 14 for header size, 4095 for last recv() next frame bytes
					if ($array['a'] > 2147479538) {
						$this->sendClientClose($client->socket, self::WS_STATUS_MESSAGE_TOO_BIG);
						return false;
					}

					// store frame payload data length
					$client->framePayloadDataLength = $array['a'];
				}
			}

			// check if the frame's payload data length has now been stored
			if ($client->framePayloadDataLength !== false)
			{
				// check if the frame's payload data length exceeds self::WS_MAX_FRAME_PAYLOAD_RECV
				if ($client->framePayloadDataLength > self::WS_MAX_FRAME_PAYLOAD_RECV)
				{
					$client->framePayloadDataLength = false;
					$this->sendClientClose($client->socket, self::WS_STATUS_MESSAGE_TOO_BIG);
					return false;
				}

				// check if the message's payload data length exceeds 2,147,483,647 or self::WS_MAX_MESSAGE_PAYLOAD_RECV
				// doesn't apply for control frames, where the payload data is not internally stored
				$controlFrame = (ord(substr($client->frameBuffer, 0, 1)) & 8) == 8;
				if (!$controlFrame) {
					$newMessagePayloadLength = $client->messageBufferLength + $client->framePayloadDataLength;
					if ($newMessagePayloadLength > self::WS_MAX_MESSAGE_PAYLOAD_RECV || $newMessagePayloadLength > 2147483647)
					{
						$this->sendClientClose($client->socket, self::WS_STATUS_MESSAGE_TOO_BIG);
						return false;
					}
				}

				return true;
			}
		}

		return false;
	}

	/**
	 * Sends message to client
	 *
	 * @param
	 */
	function send($socket, $message, $binary=false)
	{
		$client = $this->findClientBySocket($socket);

		return $this->sendClientMessage($client->socket,
			$binary ? self::WS_OPCODE_BINARY : self::WS_OPCODE_TEXT,
			$message
		);
	}

	/**=======================================
	 * HANDSHAKING
	 *========================================*/

	/**
	 * Validates socket request when new socket is connected.
	 *
	 * - check request method. Accept method
	 * - check websocket version
	 *
	 * - check request origin. Accepted origin. (Same Origin Policy)
	 * - check allowed origins
	 * - check denied ips
	 *
	 * @see 	https://developer.mozilla.org/en-US/docs/WebSockets/Writing_WebSocket_servers#Client_Handshake_Request
	 *
	 * @return 	bool	TRUE on success, FALSE on failure
	 */
	private function _validateRequest(Request $request)
	{
		// Request method must be GET
		if (strtoupper($request->getMethod()) != 'GET')
			return false;

		$headers = $request->getHeaders();
		
		// Check Host header was received
		if (!isset($headers['Host']))
			return false;

		// check Sec-WebSocket-Key header was received
		// and decoded value length is 16
		if (!isset($headers['Sec-WebSocket-Key'])
		|| strlen(base64_decode($headers['Sec-WebSocket-Key'])) != 16)
			return false;

		// Check WebSocket version
		if (!isset($headers['Sec-WebSocket-Version'])
			|| (int) $headers['Sec-WebSocket-Version'] < 7)
			return false;
		
		return true;
	}
	
	/**
	 * Creates handshake
	 */
	function processClientHandshake($socket, &$buffer)
	{
		$client = $this->findClientBySocket($socket);
		
		// End of headers
		$eoh = strpos($buffer, "\r\n\r\n");
		if (!$eoh) return false;

		// Process header lines
		$headers = explode("\r\n", substr($buffer, 0, $eoh));

		// Get REQUEST line
		$request = explode(' ', $headers[0]);
		$method = $request[0];
		$path = $request[1];
		$protocol = $request[2];
		$request = new Request($socket, $method, $path, $protocol);
		
		// Populate request headers
		foreach($headers as $header)
		{
			$header = trim($header);
			if(preg_match('/\A(\S+): (.*)\z/', $header, $matches))
			{
				$request->setHeader(trim($matches[1]), trim($matches[2]));
			}
		}

		/*
		echo $request->toString();
		die;
		*/
		// Cache client request
		// $client->request = $request;
		
		// Validate Request
		if(!$this->_validateRequest($request))
		{
			return false;
		}

		// Build handshake response
		$response = new Response(101);
		$acceptKey = base64_encode(sha1($request->getHeader('Sec-WebSocket-Key').$this->_acceptKey, true));
		$response->setHeader('Sec-WebSocket-Accept', $acceptKey);
		$response->setHeader('Upgrade', 'websocket');
		$response->setHeader('Connection', 'Upgrade');
		/*
		$response->setHeader('WebSocket-Location', 'ws://' . gethostbyaddr($this->_address));
		$response->setHeader('WebSocket-Origin', '');
		*/
		
		// $client->request = $request;

		// Send accept headers to client
		$accept = $response->toString();
		$left = strlen($accept);
		do {
			$sent = @socket_send($client->socket, $accept, $left, 0);
			if ($sent === false) return false;

			$left -= $sent;
			if ($sent > 0) $accept = substr($accept, $sent);
		}
		while ($left > 0);

		return true;
	}
	
	/**============================================
	 * LOGGING
	 *===============================================*/

	/**
	 *
	 */
	function log( $message )
	{
		echo date('Y-m-d H:i:s: ') . $message . "\n";
	}
	
	/**========================================
	 * SHUTDOWN FUNCTIONS
	 *================================================*/
	 
	/**
	 *
	 */
	function shutdown()
	{
		// check if server is not running
		if (!isset($this->_socket)) return false;

		// close all client connections
		foreach ($this->_clients as $client)
		{
			// if the client's opening handshake is complete, tell the client the server is 'going away'
			if ($client->readyState != self::WS_READY_STATE_CONNECTING)
			{
				$this->sendClientClose($client->socket, self::WS_STATUS_GONE_AWAY);
			}
			socket_close($client->socket);
		}

		// close the socket which listens for incoming clients
		socket_close($this->_socket);

		// reset variables
		$this->clients = array();

		return true;
	}

}
