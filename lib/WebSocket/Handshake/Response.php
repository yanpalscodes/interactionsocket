<?php
/**
 * Mule Framework (WebSocket)
 */
namespace Mf\WebSocket\Handshake;

/**
 *
 */
class Response
{
	// @var array HTTP response codes and messages
    protected static $_statusCodes = array
	(
        //Informational 1xx
        100 => '100 Continue',
        101 => '101 Switching Protocols',
        //Successful 2xx
        200 => '200 OK',
        201 => '201 Created',
        202 => '202 Accepted',
        203 => '203 Non-Authoritative Information',
        204 => '204 No Content',
        205 => '205 Reset Content',
        206 => '206 Partial Content',
        //Redirection 3xx
        300 => '300 Multiple Choices',
        301 => '301 Moved Permanently',
        302 => '302 Found',
        303 => '303 See Other',
        304 => '304 Not Modified',
        305 => '305 Use Proxy',
        306 => '306 (Unused)',
        307 => '307 Temporary Redirect',
        //Client Error 4xx
        400 => '400 Bad Request',
        401 => '401 Unauthorized',
        402 => '402 Payment Required',
        403 => '403 Forbidden',
        404 => '404 Not Found',
        405 => '405 Method Not Allowed',
        406 => '406 Not Acceptable',
        407 => '407 Proxy Authentication Required',
        408 => '408 Request Timeout',
        409 => '409 Conflict',
        410 => '410 Gone',
        411 => '411 Length Required',
        412 => '412 Precondition Failed',
        413 => '413 Request Entity Too Large',
        414 => '414 Request-URI Too Long',
        415 => '415 Unsupported Media Type',
        416 => '416 Requested Range Not Satisfiable',
        417 => '417 Expectation Failed',
        418 => '418 I\'m a teapot',
        422 => '422 Unprocessable Entity',
        423 => '423 Locked',
        //Server Error 5xx
        500 => '500 Internal Server Error',
        501 => '501 Not Implemented',
        502 => '502 Bad Gateway',
        503 => '503 Service Unavailable',
        504 => '504 Gateway Timeout',
        505 => '505 HTTP Version Not Supported'
    );
	
	// Http status Example: HTTP/1.1 101 Switching Protocols
	protected $_status;
	
	// @var 	Http protocol
	protected $_protocol;
	
	// @var 	Http headers. $headers[key] => $value
	protected $_headers = array();
	
	/**
	 *
	 */
	public function __construct($code, $protocol = 'HTTP/1.1')
	{
		$this->setStatus($code);
		$this->_protocol = $protocol;
	}
	
	/**
	 *
	 */
	public function setStatus($code)
	{
		$code = (int) $code;
		if(array_key_exists($code, self::$_statusCodes))
		{
			$this->_status = self::$_statusCodes[$code];
			return true;
		}
		
		return false;
	}
	
	/**
	 *
	 */
	public function setHeader($key, $value)
	{
		if(empty($key)) return false;
		if(isset($this->_headers[$key]) && empty($value))
		{
			unset($this->_headers[$key]);
			return true;
		}
		
		$this->_headers[$key] = $value;
		return true;
	}
	
	/**
	 *
	 */
	public function getHeaders()
	{
		return $this->_headers;
	}
	
	/**
	 * Creates raw response header string
	 *
	 * @param 	void
	 */
	public function toString()
	{
		// Response line
		$headers = '';
		$headers .= implode(' ', array(
			$this->_protocol,
			$this->_status
		));
		$headers .= "\r\n";

		// Merge headers, separated by \r\n
		foreach($this->_headers as $key => $value)
		{
			$value = trim($value);
			if($key && $value)
			{
				// $key: $value
				$headers .= $key .': '.$value;
				$headers .= "\r\n";
			}
		}
		
		$headers .= "\r\n";
		return $headers;
	}
	
	/**
	 *
	 */
	public function __toString()
	{
		return $this->toString();
	}
}
