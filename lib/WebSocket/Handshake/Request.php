<?php
/**
 * Mule Framework (WebSocket)
 */
namespace Mf\WebSocket\Handshake;

/**
 *
 */
class Request
{
	// @var 	string 	Request method
	protected $_method;
	
	// @var 	string 	URI path
	protected $_path;
	
	// @var 	string 	Http protocol
	protected $_protocol;
	
	// @var 	array 	$_headers[key] => value
	protected $_headers = array();
	
	// @var 	resource 	Socket making request
	protected $_socket;
	
	/**
	 * Creates a handshake Request object
	 *
	 * @param 	$socket 	resource
	 * @param 	$method 	string
	 * @param 	$path 		string
	 * @param 	$protocol 	string
	 */
	public function __construct($socket, $method = 'GET', $path='/', $protocol='HTTP/1.1')
	{		
		$this->_socket = $socket;
		$this->_method = strtoupper($method);
		$this->_path = $path;
		$this->_protocol = $protocol;
	}
	
	/**
	 *
	 */
	public function setHeader($key, $value)
	{
		if(empty($key)) return false;
		if(isset($this->_headers[$key]) && empty($value))
		{
			unset($this->_headers[$key]);
			return true;
		}
		
		$this->_headers[$key] = $value;
		return true;
	}
	
	/**
	 *
	 */
	public function getHeaders()
	{
		return $this->_headers;
	}
	
	/**
	 *
	 */
	public function getHeader($key)
	{
		$value = null;
		if(isset($this->_headers[$key]))
		{
			$value = $this->_headers[$key];
		}
		
		return $value;
	}
	
	/**
	 *
	 */
	public function getMethod()
	{
		return $this->_method;
	}
	
	/**
	 *
	 */
	public function getProtocol()
	{
		return $this->_protocol;
	}
	
	/**
	 *
	 */
	public function getPath()
	{
		return $this->_path;
	}
	
	/**
	 * Creates raw request header string
	 *
	 * @param 	void
	 */
	public function toString()
	{
		// Request line
		$headers = '';
		$headers .= implode(' ', array(
			$this->_method,
			$this->_path,
			$this->_protocol
		));
		$headers .= "\r\n";

		// Merge headers, separated by \r\n
		foreach($this->_headers as $key => $value)
		{
			$value = trim($value);
			if($key && $value)
			{
				// $key: $value
				$headers .= $key .': '.$value;
				$headers .= "\r\n";
			}
		}
		
		$headers .= "\r\n";
		return $headers;
	}
	
	/**
	 *
	 */
	public function __toString()
	{
		return $this->toString();
	}
}
