<?php
/**
 * Mule Framework (WebSocket)
 */
namespace Mf\WebSocket;
use Mf\WebSocket\Exception;

/**
 *
 */
class Client
{
	// @var 	Client socket
	public $socket = null;
	
	//
	public $ip;
	
	// @var 
	public $messageBuffer;
	
	//
	public $readyState;
	
	//
	public $lastRecvTime;
	
	//
	public $pingSentTime;
	
	//
	public $closeStatus;
	
	//
	public $framePayloadDataLength;
	
	//
	public $frameBytesRead;
	
	//
	public $frameBuffer;
	
	//
	public $messageOpcode;
	
	//
	public $messageBufferLength;
	
/*
	$wsClients[ integer ClientID ] = array(
		0 => resource  Socket,                            // client socket
		1 => string    MessageBuffer,                     // a blank string when there's no incoming frames
		2 => integer   ReadyState,                        // between 0 and 3
		3 => integer   LastRecvTime,                      // set to time() when the client is added
		4 => int/false PingSentTime,                      // false when the server is not waiting for a pong
		5 => int/false CloseStatus,                       // close status that wsOnClose() will be called with
		6 => integer   IPv4,                              // client's IP stored as a signed long, retrieved from ip2long()
		7 => int/false FramePayloadDataLength,            // length of a frame's payload data, reset to false when all frame data has been read (cannot reset to 0, to allow reading of mask key)
		8 => integer   FrameBytesRead,                    // amount of bytes read for a frame, reset to 0 when all frame data has been read
		9 => string    FrameBuffer,                       // joined onto end as a frame's data comes in, reset to blank string when all frame data has been read
		10 => integer  MessageOpcode,                     // stored by the first frame for fragmented messages, default value is 0
		11 => integer  MessageBufferLength                // the payload data length of MessageBuffer
	)
*/

	public function __construct($socket)
	{
		if(gettype($socket) != 'resource')
		{
			throw new Exception("Invalid socket resource");
		}

		// fetch client IP as integer
		if(socket_getpeername($socket, $ip) == FALSE)
		{
			throw new Exception("Could not get socket IP");
		}
		
		$this->socket = $socket;
		//$this->ip = ip2long($ip);
		$this->ip = $ip;
	}
}