NEW FUNCTIONS
- mask ($text, $opcode)
- unmask
- setLogger
- validateClientHeaders
	Sends a 404 Bad Request HTTP status
	$status = "HTTP/1.1 404 Bad Request";
	https://developer.mozilla.org/en-US/docs/WebSockets/Writing_WebSocket_servers#Client_Handshake_Request

TODO @SECURITY
- Check allowed IPs
- Check allowed origins
- Implement SAME-ORIGIN POLICY where only hosts from same domain can connect
- DdOS Prevention: Limit number of connections form one client/ip to 200
- Client Tracking: Track the number of connections made by a client/IP
   The same client IP address can try to connect multiple times
   (but the server can deny them if they attempt to many connections in order to save itself from

METHODS TO EXPOSE
- send() 						// Send message to client
- close() 						// Close client socket

ChatUser = {
	id: ''
	lastMsgSeen: '' // MessageID of message last seen by user
}

CHAT
- broadcast
- sendToGroup
