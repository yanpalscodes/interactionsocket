<?php
/**
 *
 */
namespace Mf\WebSocket\Event;
use Mf_Core\Event\Target as EventTarget;
use Mf\WebSocket\Event\Event;

/**
 *
 */
abstract class Target extends EventTarget
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @override
	 */
	public function notify($eventType, array $params = array())
	{
		foreach($this->_observers as $observer)
		{
			// Create event
			$event = new Event($eventType, $params);
			$event->setTarget($this);
			
			// Update listener of event
			$observer->update($event);
		}
	}
}