<?php
/**
 * GUID Generator
 *
 * @See		http://www.ietf.org/rfc/rfc4122.txt
 *			PHP COM and .Net (Windows) functions
 */
abstract class Guid
{
	/**
	 * Generates a GUID.
	 */
	public static function generate()
	{
		if (function_exists('com_create_guid'))
		{
			return com_create_guid();
		}

		// Prefix and suffix { and } respectively
		return chr(123)// "{"
			. self::createUUID()
			. chr(125);// "}"
	}
	
	/**
	 * Creates a custom UUID
	 */
	public static function createUUID()
	{
		mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
		$md5hash = strtoupper(md5(uniqid(rand(), true)));
		$hyphen = chr(45);// "-"
		$uuid = substr($md5hash, 0, 8).$hyphen
			.substr($md5hash, 8, 4).$hyphen
			.substr($md5hash,12, 4).$hyphen
			.substr($md5hash,16, 4).$hyphen
			.substr($md5hash,20,12);
		
		return $uuid;
	}
}
